#pragma once


class mpu6500{
  private: 
  enum{
    write=0x00,
    read=0x80
  };
 
  /*mpu6500 addrs*/
  enum{
    config=0x1A,
    gyro_config=0x1B,
    accel_config=0x1C,
    accel_config2=0x1D,
    accel_xout_h=0x3B,
    accel_xout_l=0x3C,
    accel_yout_h=0x3D,
    accel_yout_l=0x3E,
    accel_zout_h=0x3F,
    accel_zout_l=0x40,
    gyro_xout_h=0x43,
    gyro_xout_l=0x44,
    gyro_yout_h=0x45,
    gyro_yout_l=0x46,
    gyro_zout_h=0x47,
    gyro_zout_l=0x48,
    user_ctrl=0x6A,
    signal_path_reset=0x68,
    pwr_mgmt_1=0x6B,
    pwr_mgmt_2=0x6C,
    who_im_i=0x75,
    xa_offset_h=0x77,
    xa_offset_l=0x78,
    ya_offset_h=0x7A,
    ya_offset_l=0x7B,
    za_offset_h=0x7D,
    za_offset_l=0x7E,
  };
  
  uint16_t rx_data[2];

  union{
    uint16_t u_data;
    int16_t s_data;
  }data;

  public:
    /*インスタンスの参照を返すメンバ関数*/
    static mpu6500& get_instance(){
      static mpu6500 instance;
      return instance;
    }
  private:
    void SPI1_config();
    void GPIO_config();
    void cs_enable();
    void cs_disable();
    uint8_t SPI1_Send(uint16_t data);
    void write_uint8(uint8_t addrs,uint16_t command);
    uint16_t read_uint8(uint16_t addrs);
    void read_uint16(uint16_t addrs,uint16_t data[]); 
    void sense_config();
  public:
    void init();
    int16_t read_who_im_i();
    int16_t read_gyro_z();
    float read_gyro_z_omega();
    int16_t read_accel_x();
    int16_t read_accel_y();
    void read_all(int16_t& _gyro_z,int16_t& _accel_x,int16_t& _accel_y);
};
