#pragma once

#include <vector>

#include "graph.h"
#include "maze.h"

class Mouse{
  private:
    //迷路の座標と進行方向
    Coord_t coord;
    //迷路のデータ
    Maze* maze;
    
    uint8_t maze_size;
  protected:
    const char* dir_char=" ^> v   <";

  public:
    Mouse(const Maze& new_maze,int16_t width);
    ~Mouse();
    
    //壁があるか調べてあればmazeにセットする.
    void sense_set_wall();
    //壁があるか調べる
    uint8_t sense_front() const;
    uint8_t sense_right() const;
    uint8_t sense_left() const;
    //1マス前進する
    uint8_t move_forward();
    //1マス前進して停止する
    uint8_t move_forward_step();
    //右向きに回転する
    void turn_right();
    //右向きに回転して停止する
    void turn_right_step();
    //左向きに回転する
    void turn_left();
    //左向きに回転して停止する
    void turn_left_step();
    //180度回転する
    void turn_back();
    const Coord_t get_coord() const{return coord;}
    const int16_t get_coord_index() const{return coord.y*maze_size+coord.x;}
    const Maze& get_maze() const{return *(maze);}
    void set_maze(const Maze& new_maze);
    void set_wall(uint8_t dir);
    void set_coord(Coord_t _coord){coord=_coord;}
    const uint8_t get_wall() const{return maze->getChkWall(coord);}
    //void print_wall() const;
    void clear_maze();
    /*マウスの方向→迷路に対する方向に変換する*/
    uint8_t trans_direction(const uint8_t local) const;
    const Route search_maze(int16_t start,int16_t goal,dijkstra_mode_t is_searching);
};
