#pragma once

#include <stdint.h>

#define sEE_ADDRS 0x50
#define I2C_SPEED 100000
#define sEE_PAGESIZE 256
#define sEE_FLAG_TIMEOUT  ((uint32_t)0x1000)
#define sEE_LONG_TIMEOUT  ((uint32_t)(30 * sEE_FLAG_TIMEOUT))

#define sEE_MAX_TRIALS_READY  0
#define sEE_STATE_BUSY        1
#define sEE_STATE_ERROR       2

#define sEE_OK                0
#define sEE_FAIL              1

void see_lowlevel_deinit();
void see_lowlevel_init();
void see_lowlevel_dmaconfig();
void see_init();
uint32_t see_read_buffer(uint8_t* pbuffer,uint16_t read_addr,uint16_t* num_byte_to_read);
uint32_t see_write_page(uint8_t* pbuffer,uint16_t write_addr,uint8_t* num_byte_to_write);
void see_write_buffer(uint8_t* pbuffer,uint16_t write_addr,uint16_t num_byte_to_write);
uint32_t see_wait_eeprom_standby_state();
uint32_t see_timeout_usercallback();
