#include "stm32f4xx.h"
#include "stm32f4xx_conf.h"
#include "delay.h"
#include "mpu6500.h"

void mpu6500::SPI1_config(){
  SPI_InitTypeDef SPI_Struct;

  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1,ENABLE);

  SPI_Struct.SPI_Direction=SPI_Direction_2Lines_FullDuplex;
  SPI_Struct.SPI_Mode=SPI_Mode_Master;
  SPI_Struct.SPI_DataSize=SPI_DataSize_8b;
  SPI_Struct.SPI_CPOL=SPI_CPOL_High;
  SPI_Struct.SPI_CPHA=SPI_CPHA_2Edge;
  SPI_Struct.SPI_NSS=SPI_NSS_Soft|SPI_NSSInternalSoft_Set;
  SPI_Struct.SPI_BaudRatePrescaler=SPI_BaudRatePrescaler_128;
  SPI_Struct.SPI_FirstBit=SPI_FirstBit_MSB;
  SPI_Struct.SPI_CRCPolynomial=7;
  SPI_Init(SPI1,&SPI_Struct);

  SPI_Cmd(SPI1,ENABLE);
}

void mpu6500::GPIO_config(){
  GPIO_InitTypeDef GPIO_Struct;

	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA,ENABLE);
	GPIO_Struct.GPIO_Pin=GPIO_Pin_7|GPIO_Pin_6|GPIO_Pin_5;
	GPIO_Struct.GPIO_Mode=GPIO_Mode_AF;
	GPIO_Struct.GPIO_OType=GPIO_OType_PP;
	GPIO_Struct.GPIO_Speed=GPIO_Speed_100MHz;
	GPIO_Struct.GPIO_PuPd=GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOA,&GPIO_Struct);

	GPIO_PinAFConfig(GPIOA,GPIO_PinSource5,GPIO_AF_SPI1);
	GPIO_PinAFConfig(GPIOA,GPIO_PinSource6,GPIO_AF_SPI1);
	GPIO_PinAFConfig(GPIOA,GPIO_PinSource7,GPIO_AF_SPI1);

	GPIO_Struct.GPIO_Pin=GPIO_Pin_4;
	GPIO_Struct.GPIO_Mode=GPIO_Mode_OUT;
	GPIO_Struct.GPIO_OType=GPIO_OType_PP;
	GPIO_Struct.GPIO_Speed=GPIO_Speed_100MHz;
	GPIO_Struct.GPIO_PuPd=GPIO_PuPd_UP;
	GPIO_Init(GPIOA,&GPIO_Struct);

	GPIO_SetBits(GPIOA,GPIO_Pin_4);
}

void mpu6500::cs_enable(){
  GPIO_ResetBits(GPIOA,GPIO_Pin_4);
}

void mpu6500::cs_disable(){
  GPIO_SetBits(GPIOA,GPIO_Pin_4);
}

uint8_t mpu6500::SPI1_Send(uint16_t data){
  uint8_t ret;

  while(SPI_I2S_GetFlagStatus(SPI1,SPI_I2S_FLAG_TXE)==RESET);
  SPI_I2S_SendData(SPI1,data);
  while(SPI_I2S_GetFlagStatus(SPI1,SPI_I2S_FLAG_RXNE)==RESET);
  ret=SPI_I2S_ReceiveData(SPI1);

  return ret;
}

void mpu6500::write_uint8(uint8_t addrs,uint16_t command){
  cs_enable();
	SPI1_Send(addrs);
	SPI1_Send(command);
	cs_disable();
}

uint16_t mpu6500::read_uint8(uint16_t addrs){
  uint16_t data;
  cs_enable();
  SPI1_Send(addrs|read);
  data=SPI1_Send(0x00);
  cs_disable();
  return data;
}

void mpu6500::read_uint16(uint16_t addrs,uint16_t data[]){
  cs_enable();
  SPI1_Send(addrs|0x80);
  data[0]=SPI1_Send(0x00);
  data[1]=SPI1_Send(0x00);
  cs_disable();
}

void mpu6500::sense_config(){
  write_uint8(pwr_mgmt_1,0x80);   //device Reset
  Delay::delay_ms(200);
  write_uint8(signal_path_reset,0x07);    //gyro accel temp reset
  Delay::delay_ms(20);
  write_uint8(user_ctrl,0x10);    //i2c if disable
  Delay::delay_ms(10);
  write_uint8(gyro_config,0x10);  //gyro config +- 1000dps
  Delay::delay_ms(10);
  write_uint8(accel_config,0x10); //accel config +-8g
}

void mpu6500::init(){
  SPI1_config();
  GPIO_config();
  Delay::delay_ms(100);
  sense_config();  
}

int16_t mpu6500::read_who_im_i(){
  return read_uint8(who_im_i);
}

int16_t mpu6500::read_gyro_z(){
  read_uint16(gyro_zout_h,rx_data); 
  data.u_data=rx_data[0]<<8;
  data.u_data|=rx_data[1];
  cs_disable();

  return data.s_data;
}

float mpu6500::read_gyro_z_omega(){
  return read_gyro_z()*0.030517578;
}

int16_t mpu6500::read_accel_x(){
  read_uint16(accel_xout_h,rx_data); 
  data.u_data=rx_data[0]<<8; 
  data.u_data=rx_data[1];
  cs_disable();

  return data.s_data;
}

int16_t mpu6500::read_accel_y(){
  read_uint16(accel_yout_h,rx_data);
  data.u_data=rx_data[0]<<8;
  data.u_data|=rx_data[1];

  return data.s_data;
}

void mpu6500::read_all(int16_t& _gyro_z,int16_t& _accel_x,int16_t& _accel_y){
  union u_buf{
		int16_t s_data;
		uint16_t u_data;;
	};

	u_buf gyro_z_buf;
	u_buf accel_x_buf;
	u_buf accel_y_buf;

	cs_enable();
	SPI1_Send(0x47|0x80);
	gyro_z_buf.u_data=SPI1_Send(0x00)<<8;
	gyro_z_buf.u_data|=SPI1_Send(0x00);
	cs_disable();
	_gyro_z=gyro_z_buf.s_data;
	for(volatile int i=0;i<200;i++);
	cs_enable();
	SPI1_Send(0x3B|0x80);
	accel_x_buf.u_data=SPI1_Send(0x00)<<8;
	accel_x_buf.u_data|=SPI1_Send(0x00);
	cs_disable();
	for(volatile int i=0;i<400;i++);
	cs_disable();
	SPI1_Send(0x3D|0x80);
	accel_y_buf.u_data=SPI1_Send(0x00)<<8;
	accel_y_buf.u_data|=SPI1_Send(0x00);
	cs_disable();
	_accel_x=accel_x_buf.s_data;
	_accel_y=accel_y_buf.s_data;
}
