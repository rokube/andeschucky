#include <cmath>

#include "mymath.h"

const float PI=std::atan(1)*4.0;
const float DEG_TO_RAD=PI/180.0;
const float WHEEL_R=122.0;
const float GEAR_RATIO=5.714;
const float ENC_NPULSE=1024.0;
const float MOTOR_CONST=(WHEEL_R*PI/ENC_NPULSE/GEAR_RATIO*2.0);
const float CMOTOR_CONST=(GEAR_RATIO*ENC_NPULSE/WHEEL_R/PI/2.0);
//ToDo:Dを調べ直す
const float D=87.0;

