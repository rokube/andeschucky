#pragma once

#include <stdint.h>

class usart{
  private:
    static const uint32_t baud_rate=115200;
    static const uint8_t NVIC_PPriority=0;
    static const uint8_t NVIC_SubPriority=0;
    static const uint8_t data_line_size=32;
    static const uint8_t data_string_size=128;

    uint8_t rx_data;
    uint8_t tx_data[data_line_size][data_string_size];
    uint8_t tx_data_write_line_pos;
    uint8_t tx_data_read_line_pos;
    uint8_t tx_data_read_string_pos;

    void clock_enable();

    void gpio_config();

    void usart_config();

    void nvic_config();

  public:
    static usart& get_instance(){
      static usart instance;
      return instance;
    }

    void init();

    void usart_putc(const char byte);

    void usart_puts(const char* str);

    void usart_printf(const char* format,...);

    void usart3_interrupt_handler(void);
};
