#pragma once 
#include <stdint.h>
#include <queue>

#include "motor.h"
#include "position.h"
#include "functors.h"
#include "userfunctors.h"
#include "usart.h"

/*ターゲットを表すクラス*/
class Target{
  private:
    Position origin;  //マイクロマウスの座標
    Position dest;    //目的地の位置の座標
    uint16_t time;
    MotionFunctor* mf;  //マイクロマウスの動きを決める
    uint8_t wall_ctrl_state;
  public:
    Target (Position _origin,Position _dest,uint16_t _time,MotionFunctor* _mf,uint8_t _wall_ctrl_state):origin(_origin),dest(_dest),mf(_mf),time(_time),wall_ctrl_state(_wall_ctrl_state){
      mf->setParams(origin,dest,time);
    }
    Target(const Target& t){
      origin=t.origin;
      dest=t.dest;
      time=t.time;
      mf=t.mf;
      mf->setParams(origin,dest,time);
    }
    Position chec_korigin() {return mf->func(0);}
    Position check_dest() {return mf->func(time);}
    Position get_current_target(uint16_t t){ return mf->func(t); }
    Position get_dest(){ return dest; }
    MotionFunctor* getMF() { return mf; }
    void destroy(){mf->destroy(); delete mf;}
    uint16_t getTime() const {return time;}
    uint8_t is_wall_ctrl_active() const {return wall_ctrl_state;}
    uint8_t get_motion_flag() const{return mf->get_motion_flag();}
};

class machine{
  private:
    Motor motorL;
    Motor motorR;
    
    //マイクロマウスの変数
    volatile float theta_p; //(rad)
    volatile float omega_p; //(rad/s)
    volatile float x_p;     //(mm)
    volatile float y_p;     //(mm)
    volatile float v_p;     //(mm/s)
    volatile float v_xp;    //(mm/s)
    volatile float v_yp;    //(mm/s)
    volatile float v_lp;    //タイヤの左側の速度
    volatile float v_rp;    //タイヤの右側の速度
    volatile float v_rr;    //タイヤの右側の目標速度(mm/s)
    volatile float v_rl;    //タイヤの左側の目標速度(mm/s)
    volatile float cos_p;
    volatile float sin_p;

    //ターゲットの変数
    volatile float theta_t; //(rad)
    volatile float x_t;     //(mm)
    volatile float y_t;     //(mm)
    volatile float v_xt;    //(mm/s)
    volatile float v_yt;    //(mm/s)
    volatile float a_xt;    //(mm/s/s)
    volatile float a_yt;    //(mm/s/s)
    volatile float omega_t; //(rad/s)
    volatile float alpha_t; //(rad/s/s)

    //マイクロマウスとターゲットの追従偏差
    volatile float x_e;
    volatile float y_e;
    volatile float v_xe;
    volatile float v_ye;
    volatile float theta_e; 
    volatile float omega_e;
    //軌道追従制御関係の変数
    volatile float v_r;   //目標速度(mm/s)
    volatile float a_r;   //目標加速度(mm/s/s)
    volatile float omega_r; //目標角速度(rad/s)
    volatile float alpha_r; //目標角加速度(rad/s/s)
    volatile float u_1;
    volatile float u_2;
    volatile float k_p1;
    volatile float k_d1;
    volatile float k_p2;
    volatile float k_d2;
    
    //超信地旋回関係の変数
    volatile float ka_p;
    volatile float ka_d;

    uint8_t active_flag;    //マウスを動かすかどうか決める変数
    uint8_t wall_control_flag;  //壁制御を行うかのフラグ

    std::queue<Target*> target_queue; //Targetのキュー
    Position fdest;
    
    int16_t gyro_def_val;
    int16_t gyro_z;
    float gyro_zi;
    int16_t old_gyro_z;

    uint16_t time;

    uint16_t adc_def_val[4];
    uint16_t wall_th_f;   //前壁のセンサ値のしきい値
    uint16_t wall_th_s;   //横壁のセンサ値のしきい値

  public:
    static machine& get_instance(){
      static machine instance;
      return instance;
    }
  private:
    machine(){};
    machine(machine const &);
    void operator=(machine const&);
    void Timer9_config();
    void Timer5_config();
  public:
    void timer9_interrupt_handler(void);
    void init(void);
    void initialize_variables(); 
    void calibrate_sensors();
    void set_destination_in_millimeter(float _x,float _y,float _angle,uint16_t _time);
    void set_destination_in_millimeter(Position dest,uint16_t _time,MotionFunctor* mf,uint8_t is_wall_ctrl_active);
    void set_destination(float _x,float _y,float _angle,uint16_t time);
    void set_destination(Position dest,uint16_t _time,MotionFunctor* mf,uint8_t _is_wall_ctrl_active);
    void set_motor_milli_meter_per_sec(float l,float r);

    const Position getFinalDestination() const { return fdest; }
    const Position getFinalDestinationInMilliMeter() const {
      Position d;
      d.x=fdest.x;
      d.y=fdest.y;
      d.angle=fdest.angle;
      return d;
    }
    
    int getVL() const{ return motorL.getV(); }
    int getVR() const{ return motorR.getV(); }
    uint16_t get_wall_th_f(){return wall_th_f;}
    uint16_t get_wall_th_s(){return wall_th_s;}

    int16_t get_gryo_zi() const{ return gyro_zi; }
    bool is_target_queue_empty() const { return target_queue.size()==0; }
    bool is_moving() const{ return time>0; }
    
    void activate();
    void deactivate();
    void calibrateSensors();

    //gyro_z と壁センサの読み取り
    void read_sensors();

    void set_wall_ctrl(bool state){ wall_control_flag=state;}
    void set_active_flag(bool state){active_flag=state;}
};

extern "C" void MachineReadSensors();
