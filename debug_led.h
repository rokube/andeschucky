#pragma once

enum{
  All_Rest=0x00,
  LED1=0x01,
  LED2=0x02,
  LED3=0x04,
  LED4=0x08,
  LED5=0x10,
  LED6=0x20,
  All_Set=0x2F
};

void LED_Init(void);
void LED_Set(uint8_t led);
void LED_Reset(uint8_t led);
