#include <stm32f4xx.h>
#include <stm32f4xx_conf.h>

#include <eeprom.h>

void see_lowlevel_deinit(){
  GPIO_InitTypeDef gpio_struct;
  NVIC_InitTypeDef nvic_struct;
  DMA_InitTypeDef dma_struct;

  I2C_Cmd(I2C3,DISABLE);

  I2C_DeInit(I2C3);

  RCC_APB1PeriphClockCmd(I2C3,DISABLE);

  gpio_struct.GPIO_Pin=GPIO_Pin_8;
  gpio_struct.GPIO_Mode=GPIO_Mode_In;
  gpio_struct.GPIO_PuPd=GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOA,&gpio_struct);

  gpio_struct.GPIO_Pin=GPIO_Pin_9;
  GPIO_Init(GPIO_C,&gpio_struct);
 
  //I2C3のDMA1Stream4の割り込みを無効
  nvic_struct.NVIC_IRQChannel=DMA1_Stream4_IRQn;
  nvic_struct.NVIC_IRQChannelPreemptionPriority=0;
  nvic_struct.NVIC_IRQChannelSubPriority=0;
  nvic_struct.NVIC_IRQChannelCmd=DISABLE;
  NVIC_Init(&nvic_struct);

  //I2C3のDMA1Stream2の割り込みを無効
  nvic_struct.NVIC_IRQChannel=DMA1_Stream2_IRQn;
  NVIC_Init(&nvic_struct);

  //I2C3のDMAの設定をリセット
  DMA_Cmd(DMA1_Stream4,DISABLE);
  DMA_Cmd(DMA1_Stream2,DISABLE);

  DMA_DeInit(DMA1_Stream4);
  DMA_DeInit(DMA1_Stream2); 
}

void see_lowlevel_init(){
  NVIC_InitTypeDef nvic_struct;
  DMA_InitTypeDef dma_struct;

  //I2C3 DMA Txの割り込みの設定
  nvic_struct.NVIC_IRQChannel=DMA1_Stream4_IRQn;
  nvic_struct.NVIC_IRQChannelPreemptionPriority=0;
  nvic_struct.NVIC_IRQChannelSubPriority=0;
  nvic_struct.NVIC_IRQChannelCmd=ENABLE;
  NVIC_Init(&nvic_struct);

  nvic_struct.NVIC_IRQChannel=DMA1_Stream2_IRQn;
  NVIC_Init(&nvic_struct);

  //DMA1のクロックを有効
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA1,ENABLE);

  DMA_ClearFlag(DMA1_Steam4,DMA_FLAG_FEIF4|DMA_FLAG_DMEIF4|DMA_FLAG_TEIF4|DMA_FLAG_HTIF4|DMA_FLAG_TCIF4);

  DMA_Cmd(DMA1_Stream4,DISABLE);
  DMA_DeInit(DMA1_Stream4);

  dma_struct.DMA_Channel=DMA_Channel_3;
  dma_struct.DMA_PeripheralBaseAddr=(uint32_t)
  dma_struct.DMA_Memory0BaseAddr=(uint32_t)0;
  dma_struct.DMA_DIR=DMA_DIR_MemoryToPeripheral;
  dma_struct.DMA_BufferSize=0xFFFF;
  dma_struct.DMA_PeripheralInc=DMA_PeripheralInc_Disable;
  dma_struct.DMA_MemoryInc=DMA_MemoryInc_Enable;
  dma_struct.DMA_PeripheralDataSize=DMA_PeripheralDataSize_Byte;

}

void see_init(){ 
  I2C_InitTypeDef i2c_struct;
  GPIO_InitTypeDef gpio_struct;  

  //ペリフェラルのクロックを有効
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C3,ENABLE);
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA,ENABLE);
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC,ENABLE);

  //PA8をSCLに設定
  gpio_struct.GPIO_Pin=GPIO_Pin_8;
  gpio_struct.GPIO_Mode=GPIO_Mode_AF;
  gpio_struct.GPIO_Speed=GPIO_Speed_50MHz;
  gpio_struct.GPIO_OType=GPIO_OType_OD;
  gpio_struct.GPIO_PuPd=GPIO_PuPd_UP;
  
  GPIO_Init(GPIOA,&gpio_struct);

  //PC9をSDAに設定
  gpio_struct.GPIO_Pin=GPIO_Pin_9;
  GPIO_Init(GPIOC,&gpio_struct);

  //PC8をGPIOに設定
  gpio_struct.GPIO_Pin=GPIO_Pin_8;
  gpio_struct.GPIO_Mode=GPIO_Mode_OUT;
  gpio_struct.GPIO_Speed=GPIO_Speed_50MHz;
  gpio_struct.GPIO_OType=GPIO_OType_PP;
  gpio_struct.GPIO_PuPd=GPIO_PuPd_NOPULL;

  GPIO_Init(GPIOC,&gpio_struct);

  //eepromのアクセスを許可
  GPIO_ResetBits(GPIOC,GPIO_Pin_8);
  
  //GPIOをI2C用に設定
  GPIO_PinAFConfig(GPIOA,GPIO_PinSource8,GPIO_AF_I2C3);
  GPIO_PinAFConfig(GPIOC,GPIO_PinSource9,GPIO_AF_I2C3);
  
  //I2C3の設定
  i2c_struct.I2C_ClockSpeed=I2C_SPEED;
  i2c_struct.I2C_Mode=I2C_Mode_I2C;
  i2c_struct.I2C_DutyCycle=I2C_DutyCycle_2;
  i2c_struct.I2C_OwnAddress1=0x00;
  i2c_struct.I2C_Ack=I2C_Ack_Disable;
  i2c_struct.I2C_AcknowledgedAddress_7bit=I2C_AcknowledgedAddress_7bit;
  
  I2C_Init(I2C3,&i2c_struct);

  I2C_Cmd(I2C3,ENABLE);
  see_lowlevel_init();

}
