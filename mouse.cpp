#include <vector>
#include <stdint.h>
#include <unistd.h>

#include "mouse.h"
#include "graph.h"
#include "adc.h"
#include "machine.h"
#include "usart.h"
#include "delay.h"
#include "mymath.h"

static const int16_t slr=40;
static const int16_t slrf=90;

typedef enum{
  still,
  straight,
  turn,
  turn_and_straight
}Motion;

static Motion prev_motion=still;

Mouse::Mouse(const Maze& new_maze,int16_t _maze_size){
  usart::get_instance().
    usart_printf("Mouse Constracta\r\n");
  Delay::delay_ms(10);
  maze_size=_maze_size;
  coord.dir=NORTH;
  coord.x=0;
  coord.y=maze_size-1;
  usart::get_instance().
    usart_printf("x:%d y:%d dir:%d\r\n",coord.x,coord.y,coord.dir);
  maze=new Maze(new_maze);
}

Mouse::~Mouse(){
  usart::get_instance().
    usart_printf("Mouse destlacta\r\n");
  delete maze;
}

void Mouse::sense_set_wall(){
  ADC_ReadAll();
  if(sense_right()){
    usart::get_instance().usart_printf("sense right\r\n");
    set_wall(trans_direction(Maze::dir_right)); 
  }
  if(sense_left()){
    usart::get_instance().usart_printf("sense left\r\n");
    set_wall(trans_direction(Maze::dir_left)); 
  }
  if(sense_front()){
    usart::get_instance().usart_printf("sense front\r\n");
    set_wall(trans_direction(Maze::dir_front)); 
  }
  //print_wall();
}

uint8_t Mouse::sense_front() const{
  uint8_t dir=NORTH;
  maze->setChkWall(coord,trans_direction(dir)<<4);
  if((ADC_ConvVal[0]+ADC_ConvVal[3]+ADC_ConvVal[4]+ADC_ConvVal[7])
      >machine::get_instance().get_wall_th_f()){
    return 1; 
  }
  else{
    return 0; 
  }
}

uint8_t Mouse::sense_right() const{
  uint8_t dir=EAST;
  maze->setChkWall(coord,trans_direction(dir)<<4);
  if(ADC_ConvVal[1]+ADC_ConvVal[5]>
      machine::get_instance().get_wall_th_s()){
    return 1;
  }
  else{
    return 0; 
  }
}

uint8_t Mouse::sense_left() const{
  uint8_t dir=WEST;
  maze->setChkWall(coord,trans_direction(dir)<<4);
  if((ADC_ConvVal[2]+ADC_ConvVal[6])>
      machine::get_instance().get_wall_th_s()){
    return 1; 
  }
  else{
    return 0; 
  }
}

void Mouse::set_maze(const Maze& new_maze){
  delete maze;
  maze=new Maze(maze_size);
}

void Mouse::clear_maze(){
  int8_t width=maze->getMazeSize();
  delete maze;
  maze=new Maze(width);
}

void Mouse::set_wall(uint8_t dir){
  maze->setWall(coord,dir);
}

uint8_t Mouse::trans_direction(const uint8_t local) const{
  uint8_t global=coord.dir;
  uint8_t dir=0;
  if(local==NORTH){
    return global; 
  }
  else if(local==EAST){
    dir=(((global&0x08)>>3)|(global<<1))&0x0f;
    return dir;
  }
  else if(local==WEST){
    dir=(((global&0x01)<<3)|(global>>1))&0x0f;
    return dir;
  }
  else{
    return dir; 
  }
}

uint8_t Mouse::move_forward_step(){
  coord.x+=!!(coord.dir&EAST)-!!(coord.dir&WEST);
  coord.y+=!!(coord.dir&SOUTH)-!!(coord.dir&NORTH);
  uint8_t d=coord.dir;  
  //NORTH:90.0,EAST:0,WEST:-180,SOUTH:-90 
  int16_t angle=((d&SOUTH)>>2)*(-90)+((d&WEST)>>3)*(-180)+(d&NORTH)*90;
  Position p(coord.x*180.0,(maze_size-coord.y-1)*180.0,float(angle)*DEG_TO_RAD);  //目的地の座標
 
  machine::get_instance().set_destination_in_millimeter(
      p,3000,new MotionLiner(new EasingPoly3()),false);

  prev_motion=straight;
  return true;
}

void Mouse::turn_right(){
  coord.dir=(coord.dir<<1)&0xF;
  if(coord.dir==0)coord.dir=0;
  uint8_t d=coord.dir;
  
  //NORTH:90.0,EAST:0,WEST:-180,SOUTH:-90
  int16_t angle=((d&SOUTH)>>2)*(-90)+((d&WEST)>>3)*(-180)+(d&NORTH)*90;
  Position p=machine::get_instance().getFinalDestinationInMilliMeter();
  p.angle=angle;
  switch(prev_motion){
    case still:
    case turn_and_straight:
      machine::get_instance().set_destination_in_millimeter(p,1500,new MotionRotation(new EasingPoly3()),false);
      break;
    case turn:
    case straight:
      machine::get_instance().set_destination_in_millimeter(p,1500,new MotionRotation(new EasingPoly3()),false);
      break;
  }
  prev_motion=turn;
}

void Mouse::turn_right_step(){
  coord.dir=(coord.dir<<1)&0xF;
  if(coord.dir==0)coord.dir=0;
  uint8_t d=coord.dir;
  int16_t angle=((d&SOUTH)>>2)*(-90)+((d&WEST)>>3)*(-180)+(d&NORTH)*90;
  float angle_rad=(float)angle*DEG_TO_RAD;

  Position p=machine::get_instance().getFinalDestinationInMilliMeter();
  p.angle=angle_rad;
  
  machine::get_instance().set_destination_in_millimeter(
      p,2500,new MotionRotation(new EasingPoly3()),false);
  prev_motion=turn;
}

void Mouse::turn_left(){
  coord.dir=(coord.dir>>1)&0xF;
  if(coord.dir==0)coord.dir=0x8;
  uint8_t d=coord.dir;
  int16_t angle=((d&SOUTH)>>2)*(-90)+((d&WEST)>>3)*(-180)+(d&NORTH)*90;
  Position p=machine::get_instance().getFinalDestinationInMilliMeter();
  p.angle=angle;
  switch(prev_motion){
    case still:
    case turn_and_straight:
      machine::get_instance().set_destination_in_millimeter(p,500,new MotionRotation(new EasingPoly3()),false);
      break; 
    case turn:
    case straight:
      machine::get_instance().set_destination_in_millimeter(p,500,new MotionRotation(new EasingPoly3()),false);
  }
  prev_motion=turn;
}

void Mouse::turn_left_step(){
  coord.dir=(coord.dir>>1)&0xF;
  if(coord.dir==0)coord.dir=0x8;
  uint8_t d=coord.dir;
  int16_t angle=((d&2)>>1)*(-90)+((d&4)>>2)*(-180)+((d&8)>>3)*90;
  float angle_rad=(float)angle*DEG_TO_RAD;

  Position p=machine::get_instance().getFinalDestinationInMilliMeter();
  p.angle=angle_rad;

  machine::get_instance().set_destination_in_millimeter(
      p,2500,new MotionRotation(new EasingPoly3()),false);
  prev_motion=turn;
}

void Mouse::turn_back(){
  coord.dir=((coord.dir&0x3)<<2)|((coord.dir&0xc)>>2);
  uint8_t d=coord.dir;
  int16_t angle=((d&2)>>1)*(-90)+((d&4)>>2)*(-180)+((d&8)>>3)*90;
  float angle_rad=(float)angle*DEG_TO_RAD;
  Position p=machine::get_instance().getFinalDestinationInMilliMeter();
  p.angle=angle;
  machine::get_instance().set_destination_in_millimeter(
      p,2500,new MotionRotation(new EasingPoly3()),false);
  prev_motion=turn;
  usart::get_instance().usart_printf("Turn Back\r\n");
}

const Route Mouse::search_maze(int16_t start,int16_t goal,dijkstra_mode_t is_searching){
  int16_t index;
  Route route;
   
  while((index=get_coord_index())!=goal){
    Coord_t coord=get_coord();

    Graph graph(get_maze());
    usart::get_instance().usart_printf("Graph Generation\r\n");
    graph.dijkstra(goal,index,is_searching);
    usart::get_instance().usart_printf("Solved Route\r\n");
    route=graph.getRoute(goal,index);

    for(Route::iterator it=route.begin()+1;it!=route.end();it++){
      uint8_t route_dir=0;

      if((*it)==(index-maze_size)){
        route_dir=NORTH; 
      }
      else if((*it)==(index+1)){
        route_dir=EAST; 
      }
      else if((*it)==(index+maze_size)){
        route_dir=SOUTH; 
      }
      else if((*it)==(index-1)){
        route_dir=WEST; 
      }
      else{
        return Route(); 
      }

      if(is_searching)sense_set_wall();

      if(route_dir==trans_direction(Maze::dir_front)){
        if(sense_front()) break;
        else{
          usart::get_instance().usart_printf("move_forward\r\n");
          move_forward_step();
        }
      }
      else if(route_dir==trans_direction(Maze::dir_right)){
        if(sense_right()) break;
        else{
          usart::get_instance().usart_printf("turn right\r\n");
          turn_right_step();
          move_forward_step();
        }
      }
      else if(route_dir==trans_direction(Maze::dir_left)){
        if(sense_left()) break;
        else{
          usart::get_instance().usart_printf("turn left\r\n");
          turn_left_step();
          move_forward_step();
        }
      }
      else{
        usart::get_instance().usart_printf("turn back\r\n");
        turn_back();
        move_forward_step();
      }
      if(is_searching){
        while(!machine::get_instance().is_target_queue_empty()); 
      }
      index=get_coord_index();
      usart::get_instance().
        usart_printf(
            "goal:%d,index:%d\r\n",goal,index);
    }
  }
  return route;
}


