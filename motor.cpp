#include "stm32f4xx.h"
#include "stm32f4xx_conf.h"
#include "delay.h"
#include "motor.h"
#include "mymath.h"
#include "usart.h"

void Motor::Tim3_encoder_init(){
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB,ENABLE);

  GPIO_PinAFConfig(GPIOB,GPIO_PinSource4,GPIO_AF_TIM3);
  GPIO_PinAFConfig(GPIOB,GPIO_PinSource5,GPIO_AF_TIM3);

  GPIO_InitTypeDef GPIO_Struct;
  GPIO_Struct.GPIO_Pin=GPIO_Pin_4|GPIO_Pin_5;
  GPIO_Struct.GPIO_Mode=GPIO_Mode_AF;
  GPIO_Struct.GPIO_OType=GPIO_OType_PP;
  GPIO_Struct.GPIO_PuPd=GPIO_PuPd_UP;
  GPIO_Struct.GPIO_Speed=GPIO_Speed_100MHz;

  GPIO_Init(GPIOB,&GPIO_Struct);

  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3,ENABLE);

  TIM_EncoderInterfaceConfig(
      TIM3,
      TIM_EncoderMode_TI12,
      TIM_ICPolarity_Rising,
      TIM_ICPolarity_Rising);
  TIM_Cmd(TIM3,ENABLE);
}

void Motor::Tim4_encoder_init(){
  GPIO_PinAFConfig(GPIOB,GPIO_PinSource6,GPIO_AF_TIM4);
  GPIO_PinAFConfig(GPIOB,GPIO_PinSource7,GPIO_AF_TIM4);

  GPIO_InitTypeDef GPIO_Struct;
  GPIO_Struct.GPIO_Pin=GPIO_Pin_6|GPIO_Pin_7;
  GPIO_Struct.GPIO_Mode=GPIO_Mode_AF;
  GPIO_Struct.GPIO_OType=GPIO_OType_PP;
  GPIO_Struct.GPIO_PuPd=GPIO_PuPd_UP;
  GPIO_Struct.GPIO_Speed=GPIO_Speed_100MHz;

  GPIO_Init(GPIOB,&GPIO_Struct);

  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4,ENABLE);
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA,ENABLE);

  TIM_EncoderInterfaceConfig(
      TIM4,
      TIM_EncoderMode_TI12,
      TIM_ICPolarity_Rising,
      TIM_ICPolarity_Rising);
  TIM_Cmd(TIM4,ENABLE);
}

void Motor::Tim2_motor_init(){
	GPIO_InitTypeDef GPIO_Struct;

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2,ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA,ENABLE);

	GPIO_Struct.GPIO_Pin=GPIO_Pin_0|GPIO_Pin_1|GPIO_Pin_2|GPIO_Pin_3;;
	GPIO_Struct.GPIO_Mode=GPIO_Mode_AF;
	GPIO_Struct.GPIO_Speed=GPIO_Speed_100MHz;
	GPIO_Struct.GPIO_OType=GPIO_OType_PP;
	GPIO_Struct.GPIO_PuPd=GPIO_PuPd_UP;
	GPIO_Init(GPIOA,&GPIO_Struct);

	GPIO_PinAFConfig(GPIOA,GPIO_PinSource0,GPIO_AF_TIM2);
	GPIO_PinAFConfig(GPIOA,GPIO_PinSource1,GPIO_AF_TIM2);
	GPIO_PinAFConfig(GPIOA,GPIO_PinSource2,GPIO_AF_TIM2);
	GPIO_PinAFConfig(GPIOA,GPIO_PinSource3,GPIO_AF_TIM2);

	TIM_TimeBaseInitTypeDef TimeBaseStruct;

	TimeBaseStruct.TIM_Period=500;
	TimeBaseStruct.TIM_Prescaler=1;
	TimeBaseStruct.TIM_ClockDivision=0;
	TimeBaseStruct.TIM_CounterMode=TIM_CounterMode_Up;
	TimeBaseStruct.TIM_RepetitionCounter=0;
	TIM_TimeBaseInit(TIM2,&TimeBaseStruct);

	TIM_OCInitTypeDef TIM_OCInitStruct;
	TIM_OCStructInit(&TIM_OCInitStruct);

	TIM_OCInitStruct.TIM_OCMode=TIM_OCMode_PWM1;
	TIM_OCInitStruct.TIM_OutputState=TIM_OutputState_Enable;
	TIM_OCInitStruct.TIM_Pulse=0;
	TIM_OCInitStruct.TIM_OCPolarity=TIM_OCPolarity_High;
	TIM_OC1Init(TIM2,&TIM_OCInitStruct);
	TIM_OC1PreloadConfig(TIM2,TIM_OCPreload_Enable);

	TIM_OCInitStruct.TIM_OutputState=TIM_OutputState_Enable;
	TIM_OCInitStruct.TIM_Pulse=0;
	TIM_OCInitStruct.TIM_OCPolarity=TIM_OCPolarity_High;
	TIM_OC2Init(TIM2,&TIM_OCInitStruct);
	TIM_OC2PreloadConfig(TIM2,TIM_OCPreload_Enable);

	TIM_OCInitStruct.TIM_OutputState=TIM_OutputState_Enable;
	TIM_OCInitStruct.TIM_Pulse=0;
	TIM_OCInitStruct.TIM_OCPolarity=TIM_OCPolarity_High;
	TIM_OC3Init(TIM2,&TIM_OCInitStruct);
	TIM_OC3PreloadConfig(TIM2,TIM_OCPreload_Enable);

	TIM_OCInitStruct.TIM_OutputState=TIM_OutputState_Enable;
	TIM_OCInitStruct.TIM_Pulse=0;
	TIM_OCInitStruct.TIM_OCPolarity=TIM_OCPolarity_High;
	TIM_OC4Init(TIM2,&TIM_OCInitStruct);
	TIM_OC4PreloadConfig(TIM2,TIM_OCPreload_Enable);

	TIM_ARRPreloadConfig(TIM2,ENABLE);
	TIM_Cmd(TIM2,ENABLE);
}

void Motor::set_r_milli_meter_per_sec(float val){
  r_milli_meter_per_sec=val;
}

void Motor::motor_service(){
	nowE=r_milli_meter_per_sec-(MOTOR_CONST*5.f*nowV);
	v_i+=nowE;
	v_d=nowE-prvE;
	prvE=nowE;
	u=k_p*nowE+k_d*v_d+k_i*v_i;;
	*(CCR_B)=(u<0)?(uint32_t)-u:0;
	*(CCR_F)=(u>0)?(uint32_t)u:0;
  //usart::get_instance().usart_printf
  //  ("r:%.3f nowE:%.3f\r\n",r_milli_meter_per_sec,nowE);
  //usart::get_instance().usart_printf("motor u:%.3f\r\n",u);
}

void Motor::set_motor_pid(float _k_p,float _k_i,float _k_d){
  k_p=_k_p;
  k_i=_k_i;
  k_d=_k_d;
}

int16_t Motor::read_nowLV(){
  nowX=TIM_Enc->CNT;
	nowV=(int)((long)nowX-(long)prvX+(((long)nowX-(long)prvX<-50000)?65536:0)-(((long)nowX-(long)prvX>50000)?65536:0));
  prvX=nowX;

  return nowV;
}

int16_t Motor::read_nowRV(){
  nowX=TIM_Enc->CNT;
	nowV=(int)(-((long)nowX-(long)prvX+(((long)nowX-(long)prvX<-50000)?65536:0)-(((long)nowX-(long)prvX>50000)?65536:0)));
  prvX=nowX;

  return nowV;
}
void Motor::init(){
  Tim3_encoder_init();
  Tim4_encoder_init();
  Tim2_motor_init();
}

void Motor::eStop(){
  *(CCR_B)=0;
  *(CCR_F)=0;
}
