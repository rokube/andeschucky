#pragma once

#include "position.h"
#include "usart.h"
#include "delay.h"

/*緩急の付け方を規定する関数オブジェクトのクラス*/
class EasingFunctor{
  protected:
    uint16_t time;
  public:
    EasingFunctor(){}
    EasingFunctor(uint16_t _time):time(_time){}
    EasingFunctor(const EasingFunctor& obj){
      time=obj.time; 
    }
    virtual ~EasingFunctor();
    virtual float func(uint16_t s){return (float)s;}  //ターゲットの位置
    virtual float vfunc(uint16_t s){return (float)s;} //ターゲットの速度
    virtual float afunc(uint16_t s){return (float)s;} //ターゲットの加速度
    virtual void initialize(){}
    void setTime(uint16_t _time){time=_time;}
    uint16_t getTime() const {return time;}
};

/*基本動作を表す関数オブジェクトのクラス*/
/*始点と終点のPositionから幾何学的な軌道を生成する*/
class MotionFunctor{
protected:
  Position* origin;   //マイクロマウスの実際の座標
  Position* dest;     //目的地
  uint16_t time;
  EasingFunctor* e;
  
public:
  //0:x,y方向に移動,1:その場で回転
  uint8_t motion_flag;
  MotionFunctor(){}
  MotionFunctor(Position _origin,Position _dest,uint16_t _time,EasingFunctor* _e){
    origin=new Position(_origin);
    dest=new Position(_dest);
    time=_time;
    e=_e;
  }
  MotionFunctor(EasingFunctor* _e){
    e=_e; 
  }
  MotionFunctor(const MotionFunctor& obj){
    origin=obj.origin;
    dest=obj.dest;
    time=obj.time;
    e=obj.e;
  }
  virtual ~MotionFunctor();
  void destroy(){delete origin; delete dest; delete e;}
  Position getOrigin() const {return *origin;}
  Position getDest() const {return *dest;}
  EasingFunctor* getE() const {return e;}
  void setParams(Position _origin,Position _dest,uint16_t _time){
    origin=new Position(_origin);
    dest=new Position(_dest);
    time=_time;
    e->setTime(time);
    initialize();
    e->initialize();
  }
  virtual Position func(uint16_t s){return (*origin);}
  virtual void initialize(){} 
  uint8_t get_motion_flag() const{return motion_flag;}
};
