#pragma once
#include <vector>
#include <stdint.h>
#include <iostream>
#include <iostream>

#include "maze.h"
#include "usart.h"

class Edge{
	public:
		int16_t to;     //接続先
		uint8_t cost;   //接続先に移動するコスト
		bool found;     //エッジが既知か
		void setCost(uint8_t _cost){
			cost=_cost;
		}
};

typedef std::vector<Edge> Edges;

class Node{
	private:
		int16_t index;  //区画のインデックス(左上を0)
		Edges edges;    //一つのノードのエッジ(最大で4)
		bool done;      //既知区画か
		int16_t cost;   //スタートからのコスト
		int16_t from_node;  //最小コストのノード
	public:
		Node(int16_t _index,int16_t _cost):index(_index),done(false),cost(_cost),from_node(-255){}
		bool operator<(const Node right) const{
			return cost<right.cost;
		}
		bool operator>(const Node right) const{
			return cost>right.cost;
		}

		int16_t getIndex() const{return index; }
		const Edges& getEdges() const{return edges; }
		void addEdge(int16_t to,int16_t _cost,bool found);
		void deleteEdge(int16_t to);

		bool isDone() const{return done; }
		void setDone(){done=true; }

		int16_t getCost() const{return cost; }
		void setCost(int16_t _cost){cost=_cost; }
		int16_t getFrom_node() const{return from_node; }
		void setFrom_node(int16_t _from_node){ from_node=_from_node; }

		void print_node() const;
		void setCostOfEdge(int16_t to,int16_t cost);

		void setFoundOfEdge(int16_t to,bool _found);
		bool isFoundOfEdgee(int16_t to);
};

typedef std::vector<Node> Nodes;
typedef std::vector<int16_t> Route;

enum dijkstra_mode_t{
	searching=1,
	solution=0
};

class Graph{
	private:
		Nodes nodes;
		uint8_t maze_size;
	public:
		Graph(uint8_t num);
		Graph(const Maze& maze);
		~Graph(){/*std::cout<<"delete maze"<<std::endl;*/}
		void addNode(uint16_t index,int16_t _cost);
		void addEdges(int16_t pos_a,int16_t pos_b,int16_t _cost,bool found);
		void addDirectionalEdge(int16_t from,int16_t to,int16_t _cost,bool found);
		void deleteEdge(int16_t pos_a,int16_t pos_b);
		void dijkstra(int16_t start,int16_t end,dijkstra_mode_t mode);
		const Route getRoute(int16_t start,int16_t end) const;
};
