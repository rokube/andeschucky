/*マイクロマウスの寸法*/

#pragma once

#include <stdint.h>

extern const float PI;
extern const float DEG_TO_RAD;
extern const float WHEEL_R;    //タイヤ半径
extern const float GEAR_RATIO; //ギア比(40:7)
extern const float ENC_NPULSE;  //エンコーダ分解能
extern const float MOTOR_CONST; //1パルスあたりの移動距離
inline float TRANS_NtoL(float x){return (WHEEL_R*PI*((float)(x))/ENC_NPULSE/GEAR_RATIO);}
extern const float MOTOR_CONST;
extern const float D;

#define SIGN(x) ((x>0)*2-((x)!=0))
#define ABS(x) ((x>0)?(x):-(x))
