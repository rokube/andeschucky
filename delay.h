#pragma once

namespace Delay{
	void Init();				//initialise Systick Timer

	void delay_ms(uint32_t _count);		//delay 1*count ms
}


