#include "stm32f4xx_conf.h"
#include "stm32f4xx.h"
#include "adc.h"

void DMA_Config();
void GPIO_Config();
void ADC1_CH10_CH11_Config();
void ADC2_CH12_CH13_Config();
void irLED_Config();

__IO uint16_t ADC_ConvVal[8]={};
uint8_t ADC_Status=0;

void ADC_ReadAll(){
  ADC_Enable();
	GPIO_SetBits(GPIOB,GPIO_Pin_8);
	for(volatile int i=0;i < 1000;i++);
	ADC_Read();
	GPIO_ResetBits(GPIOB,GPIO_Pin_8);
  ADC_Disable();
}

void ADC_Read(){
	ADC_RegularChannelConfig(ADC1,ADC_Channel_10,1,ADC_SampleTime_28Cycles);
	ADC_RegularChannelConfig(ADC1,ADC_Channel_11,2,ADC_SampleTime_28Cycles);
	ADC_RegularChannelConfig(ADC1,ADC_Channel_10,3,ADC_SampleTime_28Cycles);
	ADC_RegularChannelConfig(ADC1,ADC_Channel_11,4,ADC_SampleTime_28Cycles);
	ADC_RegularChannelConfig(ADC2,ADC_Channel_12,1,ADC_SampleTime_28Cycles);
	ADC_RegularChannelConfig(ADC2,ADC_Channel_13,2,ADC_SampleTime_28Cycles);
	ADC_RegularChannelConfig(ADC2,ADC_Channel_12,3,ADC_SampleTime_28Cycles);
	ADC_RegularChannelConfig(ADC2,ADC_Channel_13,4,ADC_SampleTime_28Cycles);

	ADC_SoftwareStartConv(ADC1);
	for(volatile int i=0;i<80;i++);
}

void ADC_Enable(){
	ADC_Status=1;
}

void ADC_Disable(){
	ADC_Status=0;
}

void ADC_Config(){
	ADC_CommonInitTypeDef ADC_CommonStruct;

	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA2,ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC,ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB,ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1,ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC2,ENABLE);

	DMA_Config();
	GPIO_Config();
	irLED_Config();
	
	ADC_CommonStruct.ADC_Mode=ADC_DualMode_RegSimult;
	ADC_CommonStruct.ADC_Prescaler=ADC_Prescaler_Div2;
	ADC_CommonStruct.ADC_DMAAccessMode=ADC_DMAAccessMode_1;
	ADC_CommonStruct.ADC_TwoSamplingDelay=ADC_TwoSamplingDelay_20Cycles;
	ADC_CommonInit(&ADC_CommonStruct);

	ADC1_CH10_CH11_Config();
	ADC2_CH12_CH13_Config();

	ADC_MultiModeDMARequestAfterLastTransferCmd(ENABLE);
	ADC_Cmd(ADC1,ENABLE);
	ADC_Cmd(ADC2,ENABLE);
}

void ADC1_CH10_CH11_Config(){
	ADC_InitTypeDef ADC_Struct;

	ADC_Struct.ADC_Resolution=ADC_Resolution_12b;
	ADC_Struct.ADC_ScanConvMode=ENABLE;
	ADC_Struct.ADC_ContinuousConvMode=DISABLE;
	ADC_Struct.ADC_ExternalTrigConvEdge=ADC_ExternalTrigConvEdge_None;
	ADC_Struct.ADC_ExternalTrigConv=ADC_ExternalTrigConv_T1_CC1;
	ADC_Struct.ADC_DataAlign=ADC_DataAlign_Right;
	ADC_Struct.ADC_NbrOfConversion=4;
	ADC_Init(ADC1,&ADC_Struct);
	
	ADC_RegularChannelConfig(ADC1,ADC_Channel_10,1,ADC_SampleTime_28Cycles);
	ADC_RegularChannelConfig(ADC1,ADC_Channel_11,2,ADC_SampleTime_28Cycles);
	ADC_RegularChannelConfig(ADC1,ADC_Channel_10,3,ADC_SampleTime_28Cycles);
	ADC_RegularChannelConfig(ADC1,ADC_Channel_11,4,ADC_SampleTime_28Cycles);
}

void ADC2_CH12_CH13_Config(){
	ADC_InitTypeDef ADC_Struct;

	ADC_Struct.ADC_Resolution=ADC_Resolution_12b;
	ADC_Struct.ADC_ScanConvMode=ENABLE;
	ADC_Struct.ADC_ContinuousConvMode=DISABLE;
	ADC_Struct.ADC_ExternalTrigConvEdge=ADC_ExternalTrigConvEdge_None;
	ADC_Struct.ADC_ExternalTrigConv=ADC_ExternalTrigConv_T1_CC1;
	ADC_Struct.ADC_DataAlign=ADC_DataAlign_Right;
	ADC_Struct.ADC_NbrOfConversion=4;
	ADC_Init(ADC2,&ADC_Struct);

	ADC_RegularChannelConfig(ADC2,ADC_Channel_12,1,ADC_SampleTime_28Cycles);
	ADC_RegularChannelConfig(ADC2,ADC_Channel_13,2,ADC_SampleTime_28Cycles);
	ADC_RegularChannelConfig(ADC2,ADC_Channel_12,3,ADC_SampleTime_28Cycles);
	ADC_RegularChannelConfig(ADC2,ADC_Channel_13,4,ADC_SampleTime_28Cycles);
}

void DMA_Config(){
	DMA_InitTypeDef DMA_Struct;

	DMA_Struct.DMA_Channel=DMA_Channel_0;
	DMA_Struct.DMA_Memory0BaseAddr=(uint32_t)&ADC_ConvVal;
	DMA_Struct.DMA_PeripheralBaseAddr=(uint32_t)0x40012308;
	DMA_Struct.DMA_DIR=DMA_DIR_PeripheralToMemory;
	DMA_Struct.DMA_BufferSize=8;
	DMA_Struct.DMA_PeripheralInc=DMA_PeripheralInc_Disable;
	DMA_Struct.DMA_MemoryInc=DMA_MemoryInc_Enable;
	DMA_Struct.DMA_PeripheralDataSize=DMA_PeripheralDataSize_HalfWord;
	DMA_Struct.DMA_MemoryDataSize=DMA_MemoryDataSize_HalfWord;
	DMA_Struct.DMA_Mode=DMA_Mode_Circular;
	DMA_Struct.DMA_Priority=DMA_Priority_High;
	DMA_Struct.DMA_FIFOMode=DMA_FIFOMode_Enable;
	DMA_Struct.DMA_MemoryBurst=DMA_MemoryBurst_Single;
	DMA_Struct.DMA_PeripheralBurst=DMA_PeripheralBurst_Single;

	DMA_Init(DMA2_Stream0,&DMA_Struct);
	DMA_Cmd(DMA2_Stream0,ENABLE);
}

void GPIO_Config(){
	GPIO_InitTypeDef GPIO_InitStruct;
	
	GPIO_InitStruct.GPIO_Pin=GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3;
	GPIO_InitStruct.GPIO_Mode=GPIO_Mode_AN;
	GPIO_InitStruct.GPIO_PuPd=GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOC,&GPIO_InitStruct);
}

void irLED_Config(){
	GPIO_InitTypeDef GPIO_InitStruct;

	GPIO_InitStruct.GPIO_Pin=GPIO_Pin_8;
	GPIO_InitStruct.GPIO_Mode=GPIO_Mode_OUT;
	GPIO_InitStruct.GPIO_PuPd=GPIO_PuPd_NOPULL;
	GPIO_InitStruct.GPIO_OType=GPIO_OType_PP;
	GPIO_InitStruct.GPIO_Speed=GPIO_Speed_100MHz;
	GPIO_Init(GPIOB,&GPIO_InitStruct);
	GPIO_ResetBits(GPIOB,GPIO_Pin_8);
}

