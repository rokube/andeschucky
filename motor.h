#pragma once

#include "stdint.h"
#include "stm32f4xx.h"
#include "stm32f4xx_conf.h"

class Motor{
  private:
    TIM_TypeDef* TIM_Enc;   //エンコーダ用タイマ
    __IO uint32_t* CCR_F;
    __IO uint32_t* CCR_B;
    uint16_t prvX;
    uint16_t nowX;
    float prvE;
    float nowE;
    float u;
    int16_t  prvV;
    int16_t nowV;

    float  r_milli_meter_per_sec;  //(mm/s)
    float v; 
    float  v_i;
    float v_d;

    //PID制御のゲイン
    float k_p;
    float k_i;
    float k_d;
    uint8_t pwm_offset;

    //左側のエンコーダのカウントに使う
    void Tim3_encoder_init();
    //右側のエンコーダのカウントに使う 
    void Tim4_encoder_init();
    //モータ用PWM
    void Tim2_motor_init();

  
  public:
    //コンストラクタ
    Motor(TIM_TypeDef* TIM_E,__IO uint32_t* CCR_F,__IO uint32_t* CCR_B):
      TIM_Enc(TIM_E),
      CCR_F(CCR_F),
      CCR_B(CCR_B),
      prvX(TIM_E->CNT),
      nowX(prvX),
      prvE(0),
      nowE(0),
      u(0.0),
      prvV(0),
      nowV(0),
      r_milli_meter_per_sec(0),
      v(0),v_i(0),v_d(0),
      k_p(0.0),k_i(0.0),k_d(0.0),
      pwm_offset(0){}
    Motor(){}
    ~Motor(){}

    void set_r_milli_meter_per_sec(float val);
    void motor_service();   //目標速度よりduty比を計算する
    int16_t enc_read();     //パルス数を読み取る
    void set_motor_pid(float _k_p,float _k_i,float _k_d);
    void set_motor_offset(uint8_t offset){pwm_offset=offset;}
    int getV() const{return nowV;}

    void eStop();
    
    //インスタン後に必ず実行する!!!
    void init();

    int16_t read_nowLV();
    int16_t read_nowRV();
};
