#include "stm32f4xx.h"
#include "stm32f4xx_conf.h"
#include "debug_led.h"

void LED_Init(){ 
  GPIO_InitTypeDef GPIO_Struct;

  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA,ENABLE);
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB,ENABLE);
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC,ENABLE);

  GPIO_Struct.GPIO_Pin=GPIO_Pin_13|GPIO_Pin_14|GPIO_Pin_15;
  GPIO_Struct.GPIO_Mode=GPIO_Mode_OUT;
  GPIO_Struct.GPIO_OType=GPIO_OType_PP;
  GPIO_Struct.GPIO_Speed=GPIO_Speed_100MHz;
  GPIO_Struct.GPIO_PuPd=GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOB,&GPIO_Struct);

  GPIO_Struct.GPIO_Pin=GPIO_Pin_6|GPIO_Pin_7;
  GPIO_Init(GPIOC,&GPIO_Struct);
  
  GPIO_Struct.GPIO_Pin=GPIO_Pin_11;
  GPIO_Init(GPIOA,&GPIO_Struct);
}

void LED_Set(uint8_t led){
  if(led&LED1){
    GPIO_SetBits(GPIOB,GPIO_Pin_13); 
  } 
  if(led&LED2){
    GPIO_SetBits(GPIOB,GPIO_Pin_14); 
  }
  if(led&LED3){
    GPIO_SetBits(GPIOB,GPIO_Pin_15); 
  }

  if(led&LED4){
    GPIO_SetBits(GPIOC,GPIO_Pin_6); 
  } 
  if(led&LED5){
    GPIO_SetBits(GPIOC,GPIO_Pin_7); 
  }
  if(led&LED6){
    GPIO_SetBits(GPIOA,GPIO_Pin_11); 
  }
}

void LED_Reset(uint8_t led){
  if(led&LED1){
    GPIO_ResetBits(GPIOB,GPIO_Pin_13); 
  } 
  if(led&LED2){
    GPIO_ResetBits(GPIOB,GPIO_Pin_14); 
  }
  if(led&LED3){
    GPIO_ResetBits(GPIOB,GPIO_Pin_15); 
  }

  if(led&LED4){
    GPIO_ResetBits(GPIOC,GPIO_Pin_6); 
  } 
  if(led&LED5){
    GPIO_ResetBits(GPIOC,GPIO_Pin_7); 
  }
  if(led&LED6){
    GPIO_ResetBits(GPIOA,GPIO_Pin_11); 
  }
}
