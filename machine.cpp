#include <cmath>

#include "stm32f4xx.h"
#include "stm32f4xx_conf.h"
#include "usart.h"
#include "delay.h"
#include "mpu6500.h"
#include "machine.h"
#include "mymath.h"
#include "adc.h"
#include "motor.h"

using namespace std;

extern"C"
void TIM1_BRK_TIM9_IRQHandler(void){ 
  machine::get_instance().timer9_interrupt_handler(); 
}

extern"C"
void TIM5_IRQHandler(){
  static uint8_t togle=0;
  if(TIM_GetITStatus(TIM5,TIM_IT_Update)!=RESET){
    TIM_ClearITPendingBit(TIM5,TIM_IT_Update);
    if(ADC_Status==0){
      togle=!togle;
      if(togle){
        ADC_ReadAll(); 
      }
    }
  }
}

void machine::Timer9_config(){
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM9,ENABLE);

  TIM_TimeBaseInitTypeDef Timer_Struct;
  Timer_Struct.TIM_Prescaler=(uint16_t)(SystemCoreClock/1000000)-1; //1MHz
  Timer_Struct.TIM_CounterMode=TIM_CounterMode_Up;
  Timer_Struct.TIM_Period=5000;
  Timer_Struct.TIM_ClockDivision=TIM_CKD_DIV1;
  Timer_Struct.TIM_RepetitionCounter=0;
  TIM_TimeBaseInit(TIM9,&Timer_Struct);
  TIM_Cmd(TIM9,ENABLE);

  //TIM_ITConfig(TIM9,TIM_IT_Update,ENABLE);

  NVIC_InitTypeDef NVIC_Struct;

  NVIC_Struct.NVIC_IRQChannel=TIM1_BRK_TIM9_IRQn;
  NVIC_Struct.NVIC_IRQChannelSubPriority=0;
  NVIC_Struct.NVIC_IRQChannelPreemptionPriority=0;
  NVIC_Struct.NVIC_IRQChannelCmd=ENABLE;
  NVIC_Init(&NVIC_Struct);
}

void machine::Timer5_config(){
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM9,ENABLE);

  TIM_TimeBaseInitTypeDef Timer_Struct;
  Timer_Struct.TIM_Prescaler=(uint16_t)(SystemCoreClock/500000)-1; //500kHz
  Timer_Struct.TIM_CounterMode=TIM_CounterMode_Up;
  Timer_Struct.TIM_Period=5000;
  Timer_Struct.TIM_ClockDivision=TIM_CKD_DIV1;
  Timer_Struct.TIM_RepetitionCounter=0;
  TIM_TimeBaseInit(TIM5,&Timer_Struct);
  TIM_Cmd(TIM5,ENABLE);

  //TIM_ITConfig(TIM5,TIM_IT_Update,ENABLE);

  NVIC_InitTypeDef NVIC_Struct;

  NVIC_Struct.NVIC_IRQChannel=TIM5_IRQn;
  NVIC_Struct.NVIC_IRQChannelSubPriority=0;
  NVIC_Struct.NVIC_IRQChannelPreemptionPriority=0;
  NVIC_Struct.NVIC_IRQChannelCmd=ENABLE;
  NVIC_Init(&NVIC_Struct);
}

void machine::timer9_interrupt_handler(void){
  if(TIM_GetITStatus(TIM9,TIM_IT_Update)!=RESET){
    //現在の機体の位置推定
    read_sensors();
    v_lp=MOTOR_CONST*5.0*(float)motorL.read_nowLV();
    v_rp=MOTOR_CONST*5.0*(float)motorR.read_nowRV();
    v_p=(v_lp+v_rp)/2.0;
    cos_p=cos(theta_p);
    sin_p=sin(theta_p);
    v_xp=v_p*cos_p;
    v_yp=v_p*sin_p;
    x_p+=v_xp*0.005;
    y_p+=v_yp*0.005;

    if(!is_target_queue_empty()){
      time+=5; 
      //現在のターゲット位置の取得
      Position p=target_queue.front()->get_current_target(time);

      //x,y方向に移動するとき
      if(target_queue.front()->get_motion_flag()==0){ 
        //ターゲットの状態の取得 
        x_t=p.x;
        y_t=p.y;
        v_xt=p.v_x;
        v_yt=p.v_y;
        a_xt=p.a_x;
        a_yt=p.a_y;


        //目標速度・角速度の計算
        x_e=x_t-x_p;
        v_xe=v_xt-v_xp;
        y_e=y_t-y_p;
        v_ye=v_yt-v_yp;

        u_1=a_xt+k_p1*x_e+k_d1*v_xe;
        u_2=a_yt+k_p2*y_e+k_d1*v_ye;

        a_r=u_1*cos_p+u_2*sin_p;
        v_r+=a_r;
        v_r=(v_r<0.0)?0.0:v_r;
        omega_r=((u_2*cos_p-u_1*sin_p)/(v_r<30.0?30.0:v_r));
      }

      //その場で回転するとき
      else if(target_queue.front()->get_motion_flag()==1){
        //ターゲットの状態の取得
        theta_t=p.angle;
        omega_t=p.omega;
        alpha_t=p.alpha;

        //目標角速度の計算
        theta_e=theta_t-theta_p;
        omega_e=omega_t-omega_p;

        alpha_r=alpha_t+ka_p*theta_e+ka_d*omega_e;
        omega_r+=alpha_r;
        v_r=0.0;
      }
      else{
        v_r=0.0;
        omega_r=0.0;
      }
    }
    else{
      v_r=0.0;
      omega_r=0.0;
    }

    //タイヤの目標速度を求める
    v_rr=v_r+2.0*D*omega_r;
    v_rl=v_r-2.0*D*omega_r;

    set_motor_milli_meter_per_sec(v_rl,v_rr);
    motorL.motor_service();
    motorR.motor_service();

    //ログデータの出力
    
    //usart::get_instance().usart_printf("%.5f  ",x_t);
    //usart::get_instance().usart_printf("%.5f  ",y_t);
    //usart::get_instance().usart_printf("%.5f  ",theta_t);
    //usart::get_instance().usart_printf("%.5f  ",a_xt); 
    //usart::get_instance().usart_printf("%.5f  ",a_yt); 
    //usart::get_instance().usart_printf("%.8f  ",p.v_x);
    //usart::get_instance().usart_printf("%.5f  ",x_p);
    //usart::get_instance().usart_printf("%.5f  ",y_p);
    //usart::get_instance().usart_printf("%.5f  ",v_p);
    //usart::get_instance().usart_printf("%.5f ",y_e);
    //usart::get_instance().usart_printf("%.5f \r\n",u_2);

    usart::get_instance().usart_printf("%.5f  ",v_lp);
    usart::get_instance().usart_printf("%.5f  ",v_rp);
    //usart::get_instance().usart_printf("%.7f  ",alpha_t);
    usart::get_instance().usart_printf("%.5f  ",v_r);
    usart::get_instance().usart_printf("%.5f  ",omega_r);
    //usart::get_instance().usart_printf("%.5f  ",theta_e); 
    usart::get_instance().usart_printf("%.5f\r\n",theta_p); 

    if(!is_target_queue_empty() && time==(target_queue.front()->getTime())){
      target_queue.front()->destroy();
      delete target_queue.front();
      target_queue.pop();
      set_wall_ctrl(false);
      time=0;
    }
    TIM_ClearITPendingBit(TIM9,TIM_IT_Update);
  }
}

void machine::init(){
  ADC_Config();
  calibrate_sensors();
  Timer9_config();
  //Timer5_config(); 

  motorL=Motor(TIM3,&(TIM2->CCR1),&(TIM2->CCR2));
  motorR=Motor(TIM4,&(TIM2->CCR4),&(TIM2->CCR3));
  motorR.init();

  motorR.set_motor_pid(3.5,0.01,1.5);
  motorL.set_motor_pid(3.0,0.01,1.3);

  initialize_variables();
  wall_th_f=40000;
  wall_th_s=40000;
  active_flag=false;
  wall_control_flag=false;
}


void machine::initialize_variables(){
  theta_p=90.0*DEG_TO_RAD;
  omega_p=0.0;
  x_p=0.0;
  y_p=0.0;
  v_p=0.0;
  v_rp=0.0;
  v_lp=0.0;

  v_r=0.0;
  omega_r=0.0;
  k_p1=1.2;
  k_d1=0.2;
  k_p2=1.2;
  k_d2=0.2;

  ka_p=0.4;
  ka_d=0.01;
  
  time=0;

  gyro_zi=2952.0;
  fdest=Position(0,0,theta_p);
  Delay::delay_ms(1000);
}

void machine::set_destination_in_millimeter(float _x,float _y,float _angle,uint16_t _time){
  set_destination(_x,_y,_angle,_time);
}

//目標の座標と幾何学的軌道の設定
void machine::set_destination_in_millimeter(Position dest,uint16_t _time,MotionFunctor *mf,uint8_t is_wall_ctrl_active){
  set_destination(dest,_time,mf,is_wall_ctrl_active);
}

void machine::set_destination(float _x,float _y,float _angle,uint16_t _time){
  Position dest=Position(_x,_y,_angle);
  EasingPoly3 *ep3=new EasingPoly3(_time);
  MotionLiner *ml=new MotionLiner(fdest,dest,_time,ep3);
  Target *t=new Target(fdest,dest,_time,ml,true);
  target_queue.push(t);
  fdest.setVars(_x,_y,_angle);
}

void machine::set_destination(Position dest,uint16_t _time,MotionFunctor *mf,uint8_t is_wall_ctrl_active){
  Target *t=new Target(fdest,dest,_time,mf,is_wall_ctrl_active);
  target_queue.push(t);
  fdest.setVars(dest.x,dest.y,dest.angle);
}

void machine::set_motor_milli_meter_per_sec(float l,float r){
  motorL.set_r_milli_meter_per_sec(l);
  motorR.set_r_milli_meter_per_sec(r);
}

void machine::activate(){
  if(!active_flag){
    initialize_variables();
    motorL.init();
    motorR.init();
    active_flag=true;
  }
}

void machine::deactivate(){
  if(active_flag){
    motorL.eStop();
    motorR.eStop();
    active_flag=false;
  }
}

void machine::calibrate_sensors(){
  int32_t sum=0; 
  for(int i=0;i<1024;i++){
    int16_t val=mpu6500::get_instance().read_gyro_z();
    sum+=val;
  }
  sum=sum/1024;
  gyro_def_val=sum;
  
  ADC_ReadAll();
  adc_def_val[0]=(ADC_ConvVal[0]+ADC_ConvVal[4]);
  adc_def_val[1]=(ADC_ConvVal[1]+ADC_ConvVal[5]);
  adc_def_val[2]=(ADC_ConvVal[2]+ADC_ConvVal[6]);
  adc_def_val[3]=(ADC_ConvVal[3]+ADC_ConvVal[7]);
}

void machine::read_sensors(){
  gyro_z=mpu6500::get_instance().read_gyro_z()-gyro_def_val;
  gyro_zi+=0.0025*(float)(old_gyro_z+gyro_z);    //台形則
  omega_p=gyro_z/32.8*PI/180.0;    //rad/s
  theta_p=gyro_zi/32.8*PI/180.0;  //degree
  old_gyro_z=gyro_z;
}

void machine_read_sensors(){
  machine::get_instance().read_sensors();
}
