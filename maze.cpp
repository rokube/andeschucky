#include <vector>
#include "maze.h"
#include "usart.h"

const uint8_t Maze::dir_front=0x01;
const uint8_t Maze::dir_right=0x02;
const uint8_t Maze::dir_back=0x04;
const uint8_t Maze::dir_left=0x08;

void Maze::maze_init(){
  cell_data_t cell;
  cell.wall=0;
  cell.cflag=0;

  for(int i=0;i<maze_size;i++){
    for(int j=0;j<maze_size;j++){
      cell.wall=0;
      if(i==0) cell.wall|=NORTH;
      if(j==0) cell.wall|=WEST;
      if(j==maze_size-1) cell.wall|=EAST;
      if(i==maze_size-1) cell.wall|=SOUTH;
      maze_data.push_back(cell);
    } 
  }
}

void Maze::addCell(cell_data_t data){
	maze_data.push_back(data);
}

void Maze::setWall(const Coord_t& coord,uint8_t wall){
	maze_data[coord.y*maze_size+coord.x].wall|=wall;

	if((wall&NORTH)&&coord.y!=0){
		maze_data[(coord.y-1)*maze_size+coord.x].wall|=SOUTH;
	}
	if((wall&EAST)&&(coord.x!=maze_size-1)){
		maze_data[coord.y*maze_size+coord.x+1].wall|=WEST;
	}
	if((wall&SOUTH)&&(coord.y!=(maze_size-1))){
		maze_data[(coord.y+1)*maze_size+coord.x].wall|=NORTH;
	}
	if((wall&WEST)&&(coord.x!=0)){
		maze_data[coord.y*maze_size+coord.x-1].wall|=EAST;
	}
}

void Maze::setWall(const uint16_t index,uint8_t wall){
	maze_data[index].wall|=wall;
	
	if((wall&NORTH)&&(index+1>maze_size)){
		maze_data[index-maze_size].wall|=SOUTH;
	}
	if((wall&EAST)&&((index+1%maze_size)!=0)){
		maze_data[index+1].wall|=WEST;
		//std::printf("EAST index: %d , wall: %3x\n",index,wall);
	}
	if((wall&SOUTH)&&(index<(maze_size*(maze_size-1)))){
		maze_data[index+maze_size].wall|=NORTH;
	}
	if((wall&WEST)&&((index%maze_size)!=0)){
		maze_data[index-1].wall|=EAST;
		//std::printf("WEST index: %d ,wall: %3x\n",index,wall);
	}
}

const uint8_t Maze::isChkWall(Coord_t coord,uint8_t chk_wall) const{	
	return maze_data[coord.y*maze_size+coord.x].wall&chk_wall;
}

const uint8_t Maze::getChkWall(Coord_t coord) const{
	return maze_data[coord.y*maze_size+coord.x].wall&chk_all;
}

const uint8_t Maze::is_searched_cell(int16_t index) const{
  return maze_data[index].wall==chk_all;
}

void Maze::setChkWall(Coord_t coord){
	maze_data[coord.y*maze_size+coord.x].wall|=chk_all;

	if(coord.y!=0){
		maze_data[(coord.y-1)*maze_size+coord.x].wall|=chk_SOUTH;
	}
	if(coord.x!=(maze_size-1)){
		maze_data[coord.y*maze_size+coord.x+1].wall|=chk_WEST;
	}
	if(coord.y!=(maze_size-1)){
		maze_data[(coord.y+1)*maze_size+coord.x].wall|=chk_NORTH;
	}
	if(coord.x!=0){
		maze_data[coord.y*maze_size+coord.x-1].wall|=chk_EAST;
	}
}

void Maze::setChkWall(const Coord_t& coord,uint8_t dir){
  maze_data[coord.y*maze_size+coord.x].wall|=chk_all;

  if(coord.y!=0){
    maze_data[(coord.y-1)*maze_size+coord.x].wall|=chk_SOUTH;
  }
  if(coord.x!=maze_size-1){
    maze_data[coord.y+coord.x+1].wall|=chk_WEST; 
  }
  if(coord.y!=maze_size-1){
    maze_data[(coord.y+1)*maze_size+coord.x].wall|=chk_NORTH; 
  }
  if(coord.x!=0){
    maze_data[coord.y*maze_size+coord.x-1].wall|=chk_EAST; 
  }
}

void Maze::setChkWall(uint16_t index){
	maze_data[index].wall|=chk_all;

	if((index+1)>maze_size){
		maze_data[index-maze_size].wall|=chk_SOUTH;
	}
	if((index+1%maze_size)!=0){
		maze_data[index+1].wall|=chk_WEST;
	}
	if(index<(maze_size*(maze_size-1))){
		maze_data[index+maze_size].wall|=chk_NORTH;
	}
	if((index%maze_size)!=0){
		maze_data[index-1].wall|=chk_EAST;
	}
}

void Maze::setChkWallAndWall(Coord_t coord,uint8_t wall){
	setWall(coord,wall);
	setChkWall(coord);
}

void Maze::setChkWallAndWall(uint16_t index,uint8_t wall){
	setWall(index,wall);
	setChkWall(index);
}


const uint8_t Maze::is_wall(const Coord_t& coord,uint8_t dir) const{
  return maze_data[coord.y*maze_size+coord.x].wall&dir;
}
