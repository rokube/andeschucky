#include "stm32f4xx.h"
#include "usart.h"

#include <stdio.h>
#include <stdarg.h>
#include <string.h>

extern "C"
void USART3_IRQHandler(void){
  usart::get_instance().usart3_interrupt_handler();
}

void usart::clock_enable(){
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB,ENABLE);
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3,ENABLE); 
}

void usart::gpio_config(){
  GPIO_InitTypeDef GPIO_Struct;

  GPIO_Struct.GPIO_Pin=GPIO_Pin_10;
  GPIO_Struct.GPIO_Mode=GPIO_Mode_AF;
  GPIO_Struct.GPIO_OType=GPIO_OType_PP;
  GPIO_Struct.GPIO_PuPd=GPIO_PuPd_NOPULL;
  GPIO_Struct.GPIO_Speed=GPIO_Speed_100MHz;

  GPIO_Init(GPIOB,&GPIO_Struct);

  GPIO_Struct.GPIO_Pin=GPIO_Pin_11;
  GPIO_Struct.GPIO_Mode=GPIO_Mode_AF;
  GPIO_Struct.GPIO_PuPd=GPIO_PuPd_NOPULL;

  GPIO_Init(GPIOB,&GPIO_Struct);

  GPIO_PinAFConfig(GPIOB,GPIO_PinSource10,GPIO_AF_USART3);
  GPIO_PinAFConfig(GPIOB,GPIO_PinSource11,GPIO_AF_USART3);
}

void usart::usart_config(){
  USART_InitTypeDef USART_Struct;

  USART_Struct.USART_BaudRate=baud_rate;
  USART_Struct.USART_WordLength=USART_WordLength_8b;
  USART_Struct.USART_StopBits=USART_StopBits_1;
  USART_Struct.USART_Parity=USART_Parity_No;
  USART_Struct.USART_HardwareFlowControl=
    USART_HardwareFlowControl_None;
  USART_Struct.USART_Mode=
    USART_Mode_Rx|USART_Mode_Tx;

  USART_Init(USART3,&USART_Struct);
  USART_ITConfig(USART3,USART_IT_RXNE,ENABLE);
  USART_Cmd(USART3,ENABLE);
}

void usart::nvic_config(){
  NVIC_InitTypeDef NVIC_Struct;

  NVIC_Struct.NVIC_IRQChannel=USART3_IRQn;
  NVIC_Struct.NVIC_IRQChannelPreemptionPriority=0;
  NVIC_Struct.NVIC_IRQChannelSubPriority=0;
  NVIC_Struct.NVIC_IRQChannelCmd=ENABLE;

  NVIC_Init(&NVIC_Struct);

}

void usart::init(){
  clock_enable();
  gpio_config();
  nvic_config();
  usart_config();
}

void usart::usart_putc(const char byte){
  USART_SendData(USART3,byte);
}

void usart::usart_puts(const char* str){
  strcpy((char*)tx_data[tx_data_write_line_pos],str);
  tx_data_write_line_pos++;
  tx_data_write_line_pos&=data_line_size-1;
  USART_ITConfig(USART3,USART_IT_TXE,ENABLE);
  if(tx_data_write_line_pos==tx_data_read_line_pos)
    while(tx_data_write_line_pos!=tx_data_read_line_pos);
}

void usart::usart_printf(const char* format,...){
  char strbuf[data_line_size];

  va_list vl;
  va_start(vl,format);
  vsprintf(strbuf,format,vl);
  usart_puts(strbuf);
  va_end(vl);
}

void usart::usart3_interrupt_handler(void){
  if(USART_GetITStatus(USART3,USART_IT_TXE)!=RESET){
    if(tx_data[tx_data_read_line_pos][tx_data_read_string_pos]=='\0'){
      tx_data_read_line_pos++;
      tx_data_read_line_pos&=data_line_size-1;
      tx_data_read_string_pos=0;
      if(tx_data_read_line_pos==tx_data_write_line_pos)
        USART_ITConfig(USART3,USART_IT_TXE,DISABLE);
    }
    else{
      USART_SendData(USART3,tx_data[tx_data_read_line_pos][tx_data_read_string_pos]);
      tx_data_read_string_pos++;
    }
  }
  if(USART_GetITStatus(USART3,USART_IT_RXNE)==SET){
    rx_data=USART_ReceiveData(USART3);
    USART_SendData(USART3,rx_data);
  }
}

