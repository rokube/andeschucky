#include "stm32f4xx.h"
#include "stm32f4xx_conf.h"
#include "delay.h"
#include "usart.h"
#include "mpu6500.h"
#include "machine.h"
#include "mouse.h"
#include "mymath.h"

Mouse *mouse;

int main(){
  Delay::Init();
  usart::get_instance().init();
  mpu6500::get_instance().init();
  machine::get_instance().init();
 
  Maze maze(3);
  maze.maze_init();

  uint8_t goal=4;
  mouse=new Mouse(maze,3);
  Delay::delay_ms(500); 

  TIM_ITConfig(TIM9,TIM_IT_Update,ENABLE);
  
  Route route;
  route=mouse->search_maze(6,4,searching);
  while(1);

}
