#pragma once

/*座標と角度を表すクラス*/
class Position{
  private:
  public:
    float x;  
    float y;
    float angle;
    float v_x;
    float v_y;
    float a_x;
    float a_y;
    float omega;
    float alpha;

    Position():x(0),y(0),angle(0),v_x(0),v_y(0),a_x(0),a_y(0),omega(0),alpha(0){}
    Position(const Position &p){
      x=p.x;
      y=p.y;
      angle=p.angle;
      v_x=p.v_x;
      v_y=p.v_y;
      a_x=p.a_x;
      a_y=p.a_y;
      omega=p.omega;
      alpha=p.alpha;
    }
    Position(float _x,float _y,float _angle):x(_x),y(_y),angle(_angle){}
    Position(float _x,float _y,float _angle,float _v_x):x(_x),y(_y),angle(_angle),v_x(_v_x){}
    void setVars(float _x,float _y,float _angle){x=_x;y=_y;angle=_angle;}
    Position operator- (const Position& rhs){return Position(x-rhs.x,y-rhs.y,angle-rhs.angle);}
    Position operator+ (const Position& rhs){return Position(x+rhs.x,y+rhs.y,angle+rhs.angle);}
    template <typename T> Position operator/ (const T d){
      return Position((T)x/d,(T)y/d,(T)angle/d);
    }
    template <typename T> Position operator* (const T d){ 
      return Position((T)x*d,(T)y*d,(T)angle*d);
    }

};
