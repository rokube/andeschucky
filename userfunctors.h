#pragma once

#include "mymath.h"
#include "position.h"

#include <vector>
#include <cmath>

class EasingPoly3 : public EasingFunctor{
  public:
    EasingPoly3():EasingFunctor(){}
    EasingPoly3(uint16_t _time):EasingFunctor(_time){}
    EasingPoly3(const EasingPoly3& obj){
      time=obj.time; 
    }  
    ~EasingPoly3(){}
    virtual float func(uint16_t s){
      return (-2.0*(float)s+3.0*time)*(float)(s*s)/(time*time); 
    }
    virtual float vfunc(uint16_t s){
      return (float)(-s+time)*6.0*(float)s/(float)(time*time); 
    }
    virtual float afunc(uint16_t s){
      return (float)((-2*s+time)*6.0)/(float)(time*time); 
    }
};

class EasingLinear : public EasingFunctor{
  public:
    EasingLinear():EasingFunctor(){}
    EasingLinear(uint16_t _time):EasingFunctor(_time){}
    EasingLinear(const EasingLinear& obj){
      time=obj.time; 
    }
    ~EasingLinear(){}
    virtual float func(uint16_t s){
      return (float)s; 
    }
};

class EasingPoly4 : public EasingFunctor{
  private:
    float a,b,c,d;
  public:
    EasingPoly4():EasingFunctor(){}
    EasingPoly4(float _time):EasingFunctor(_time){}
    EasingPoly4(float _time,float y0,float yt,float v0,float vt,float a0):EasingFunctor(_time/200.f){
      a=(3.0*y0+2.0*v0*time+a0*0.5*time*time+
          vt*time-3.0*yt)/time/time/time/(yt-y0);
      b=(4.0*yt-4.0*y0-vt*time-3.0*v0*time-a0*time*time)/time/time/(yt-y0);
      c=a0*0.5*time/(yt-y0);
      d=v0*time/(yt-y0);
    }
    EasingPoly4(const EasingPoly4& obj){
      time=obj.time;
      a=obj.a;
      b=obj.b;
      c=obj.c;
      d=obj.d;
    }
    ~EasingPoly4(){}
    virtual float func(unsigned int s){
      float sf=(float)s/200.0;
      return 200.0*(a*sf*sf*sf*sf+b*sf*sf*sf+c*sf*sf+d*sf);
    }
};


//幾何学的軌道をきめる
//直進
class MotionLiner : public MotionFunctor{
  private:
    Position diff; //目標の状態との誤差
    Position target; //ターゲット
  public:
    MotionLiner(EasingFunctor *_e):MotionFunctor(_e){
      motion_flag=0; 
    }
    MotionLiner(
        Position _origin,
        Position _dest,
        uint16_t _time,
        EasingFunctor *_e):
      MotionFunctor(_origin,_dest,_time,_e){
    motion_flag=0;
    diff=*dest-*origin; 
  }
    MotionLiner(const MotionLiner& obj){
      origin=obj.origin;
      dest=obj.dest;
      diff=*dest-*origin;
      time=obj.time;
      e=obj.e;
      motion_flag=obj.motion_flag;
    }
    ~MotionLiner(){}
    //時刻sにおけるターゲットのPositionを計算
    virtual Position func(uint16_t s){
      float t=e->func(s);
      float dt=e->vfunc(s);
      float ddt=e->afunc(s);
      
      target.x=origin->x+(diff.x*(t/(float)time));
      target.y=origin->y+(diff.y*(t/(float)time));
      target.v_x=dt*(diff.x/(float)time); 
      target.v_y=dt*(diff.y/(float)time);
      target.a_x=ddt*(diff.x/(float)time);
      if(std::isnan(target.a_x)) target.a_x=0.0;
      target.a_y=ddt*(diff.y/(float)time);
      if(std::isnan(target.a_y)) target.a_y=0.0;
      return target;
    }
    virtual void initialize(){
      diff=*dest-*origin; 
    }
};

class MotionRotation : public MotionFunctor{
  private:
    Position diff; //目標の状態との誤差
    Position target;  //ターゲット
  public:
    MotionRotation(EasingFunctor* _e):MotionFunctor(_e){
      motion_flag=1; 
    }
    MotionRotation(
        Position _origin,
        Position _dest,
        uint16_t _time,
        EasingFunctor* _e)
      :MotionFunctor(_origin,_dest,_time,_e){
        diff=*dest-*origin; 
        motion_flag=1;
      }
    MotionRotation(const MotionRotation& obj){
      origin=obj.dest;
      dest=obj.dest;
      diff=*dest-*origin;
      time=obj.time;
      e=obj.e;
      motion_flag=1;
    }
    //デストラクタ
    ~MotionRotation(){}
    //時刻sにおけるターゲットのPositionを計算
    virtual Position func(uint16_t s){
      float t=e->func(s);
      float dt=e->vfunc(s);
      float ddt=e->afunc(s);

      target=*origin+(diff*(t/(float)time));
      target.omega=dt*(diff.angle/(float)time);
      target.alpha=ddt*(diff.angle/(float)time);

      return target;
    }
    virtual void initialize(){
      diff=*dest-*origin; 
    }
};
