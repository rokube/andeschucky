SHELL = /bin/sh
TARGET_ARCH   = -mcpu=cortex-m4 -mthumb -mfloat-abi=softfp -mfpu=fpv4-sp-d16 
INCLUDE_DIRS  = -I /home/koki/workspace/Libraries \
				-I /home/koki/workspace/Libraries/STM32F4xx_StdPeriph_Driver/inc \
				-I /home/koki/workspace/Libraries/CMSIS/Device/ST/STM32F4xx/Include \
				-I /home/koki/workspace/Libraries/CMSIS/Include 
STARTUP_DIR = /home/koki/workspace/Libraries/CMSIS/Device/ST/STM32F4xx/Source/Templates/gcc_ride7/
BOARD_OPTS = -DHSE_VALUE=\(\(uint32_t\)12000000\) -DSTM32F4XX
FIRMWARE_OPTS = -DUSE_STDPERIPH_DRIVER
COMPILE_OPTS  = -std=c++11 -Os -g3 -ffunction-sections -fpermissive -fdata-sections -fsigned-char -fno-rtti -fexceptions -Wall -fmessage-length=0 $(INCLUDE_DIRS) $(BOARD_OPTS) $(FIRMWARE_OPTS)

CC      = arm-none-eabi-g++
CXX		= $(CC)
AS      = $(CC)
LD      = $(CC)
AR      = arm-none-eabi-ar
OBJCOPY = arm-none-eabi-objcopy
CFLAGS  = $(COMPILE_OPTS) 
CXXFLAGS= $(COMPILE_OPTS)
ASFLAGS = -x assembler-with-cpp -c $(TARGET_ARCH) $(COMPILE_OPTS) 
LDFLAGS = -Wl,--gc-sections,-Map=bin/main.map,-cref -T stm32_flash.ld $(INCLUDE_DIRS) -lstdc++ -L /home/koki/workspace/Libraries 
all: libstm32f4xx startup bin/main.hex

# main.o is compiled by suffix rule automatucally
bin/main.hex: $(patsubst %.c,%.o,$(wildcard *.c)) $(patsubst %.cpp,%.o,$(wildcard *.cpp)) $(STARTUP_DIR)startup_stm32f4xx.o /home/koki/workspace/Libraries/libstm32f4xx.a 
	$(LD) $(LDFLAGS) $(TARGET_ARCH) $^ -o bin/main.elf 
	$(OBJCOPY) -O ihex bin/main.elf bin/main.hex

# many of xxx.o are compiled by suffix rule automatically
LIB_OBJS = $(sort \
 $(patsubst %.c,%.o,$(wildcard /home/koki/workspace/Libraries/STM32F4xx_StdPeriph_Driver/src/*.c)))

libstm32f4xx: $(LIB_OBJS)
	$(AR) cr /home/koki/workspace/Libraries/libstm32f4xx.a $(LIB_OBJS)
	
startup:
	$(AS) -o $(STARTUP_DIR)/startup_stm32f4xx.o $(ASFLAGS) $(STARTUP_DIR)startup_stm32f4xx.s

$(LIB_OBJS): \
 $(wildcard /home/koki/workspace/Libraries/STM32F4xx_StdPeriph_Driver/inc/*.h) \
 $(wildcard /home/koki/workspace/Libraries/STM32F4xx_StdPeriph_Driver/src/*.c) \
 makefile

clean:
	rm -rf *.o *.s bin\*
