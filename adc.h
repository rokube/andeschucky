#pragma once

void ADC_Config();
void ADC_Read();
void ADC_ReadAll();
void ADC_Enable();
void ADC_Disable();

extern volatile uint16_t ADC_ConvVal[8];
extern uint8_t ADC_Status;

