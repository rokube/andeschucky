#include <vector>
#include <algorithm>
#include <queue>
#include "graph.h"
#include "maze.h"

#include "usart.h"
#include "delay.h"

void Node::addEdge(int16_t to,int16_t _cost,bool found){
  Edge edge;
  edge.to=to;
  edge.cost=_cost;
  edge.found=found;
  edges.push_back(edge);
}

void Node::deleteEdge(int16_t to){
  for(Edges::iterator it=edges.begin();it!=edges.end();){
    if(it->to==to){
      it=edges.erase(it);
      continue;
    }
    ++it;
  }
}

void Node::setCostOfEdge(int16_t to,int16_t cost){
  for(Edges::iterator it=edges.begin();it!=edges.end();++it){
    if((it->to==to)&&((it->cost)<cost))
      it->setCost(cost);
  }
}

Graph::Graph(uint8_t num):maze_size(num){}

Graph::Graph(const Maze& maze){
  maze_size=maze.getMazeSize();
  int16_t cnt=0;
  const MazeData& maze_data=maze.getMazeData();
  nodes.reserve(256);
  for(MazeData::const_iterator it=maze_data.begin();it!=maze_data.end();++it){
    addNode(cnt,-1);
    int16_t cost=1;
    if(!(((*it).wall & NORTH)==NORTH)){
      if((*it).wall&SOUTH) cost+=2;
      if((cnt-maze_size>=0)&&(maze_data[cnt-maze_size].wall & NORTH)) cost+=2;

      nodes[cnt-maze_size].setCostOfEdge(cnt,cost);
      addDirectionalEdge(cnt,cnt-maze_size,cost,((*it).wall&chk_NORTH)==chk_NORTH);
    }
    cost=1;
    if(!((*it).wall&EAST)){
      if((*it).wall&WEST) cost+=2;
      if((cnt+1<256)&&(maze_data[cnt+1].wall & EAST)) cost+=2;
      addDirectionalEdge(cnt,cnt+1,cost,((*it).wall&chk_EAST)==chk_EAST);
    }
    cost=1;
    if(!(((*it).wall & SOUTH)==SOUTH)){
      if(!((*it).wall & NORTH)) cost+=2;
      if((cnt+maze_size<256)&&(maze_data[cnt+1].wall & EAST)) cost+=2;
      addDirectionalEdge(cnt,cnt+maze_size,cost,((*it).wall&chk_SOUTH)==chk_SOUTH);
    }
    cost=1;
    if(!((*it).wall & WEST)){
      if((*it).wall & EAST) cost+=2;
      if((cnt-1>=0)&&(maze_data[cnt-1].wall & WEST))	cost+=2;
      nodes[cnt-1].setCostOfEdge(cnt,cost);
      addDirectionalEdge(cnt,cnt-1,cost,((*it).wall&chk_WEST)==chk_WEST);
    }
    Edges edges=nodes[cnt].getEdges();
    ++cnt;
  }
}

void Graph::addNode(const uint16_t index,const int16_t _cost){
  Node node(index,_cost);
  nodes.push_back(node);
}

void Graph::addEdges(int16_t pos_a,int16_t pos_b,int16_t _cost,bool found){
  nodes[pos_a].addEdge(pos_b,_cost,found);
  nodes[pos_b].addEdge(pos_a,_cost,found);
}

void Graph::addDirectionalEdge(int16_t from,int16_t to,int16_t _cost,bool found){
  nodes[from].addEdge(to,_cost,found);
}

void Graph::deleteEdge(int16_t pos_a,int16_t pos_b){
  nodes[pos_a].deleteEdge(pos_b);
  nodes[pos_b].deleteEdge(pos_a);
}

void Graph::dijkstra(int16_t start,int16_t goal,dijkstra_mode_t mode){
  nodes[start].setCost(0);

  std::priority_queue<Node*,std::vector<Node*>,std::greater<Node*> > node_queue;
  //std::priority_queue<Node*> node_queue;
  std::vector<int16_t>in_queue;
  node_queue.push(&nodes[start]);
  
  while(1){
    Node* doneNode=node_queue.top();
    node_queue.pop();
        
    for(std::vector<int16_t>::iterator it=in_queue.begin();it!=in_queue.end();){
      if((*it)==doneNode->getIndex()){
        it=in_queue.erase(it);
        continue;
      }
      ++it;
    }
    doneNode->setDone();
    Edges edges=doneNode->getEdges(); 

    for(Edges::iterator it=edges.begin();it!=edges.end();++it){
      int16_t to=it->to;
      int16_t cost=doneNode->getCost()+it->cost;
      if((it->found)||(mode==searching)){
        if(nodes[to].getCost()<0||cost<nodes[to].getCost()){
          nodes[to].setCost(cost);
          nodes[to].setFrom_node(doneNode->getIndex());
          if(find(in_queue.begin(),in_queue.end(),to)==in_queue.end()){
            node_queue.push(&nodes[to]);
            in_queue.push_back(to);
          }
        }
      }
    }
    if(doneNode->getIndex()==goal) break;
  }
}

const Route Graph::getRoute(int16_t start,int16_t end) const{
  int16_t cursor=end;
  Route route;

  while((cursor>=0)&&(cursor!=start)){
    route.push_back(cursor);
    cursor=nodes[cursor].getFrom_node();
  }
  route.push_back(start);

  return route;
}
