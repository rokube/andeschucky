#include "stm32f4xx.h"
#include "stm32f4xx_conf.h"
#include "delay.h"

extern "C"{
volatile uint32_t count=0;

void SysTick_Handler(void){
	count--;
}

}

void Delay::Init(){
	SysTick_Config(SystemCoreClock/1000);
}

void Delay::delay_ms(uint32_t _count){
	count=_count;
	while(count){
		__WFI();
	}
}
