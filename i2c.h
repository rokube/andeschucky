#pragma once

#include <stdint.h>

class EEPROM{
  private:
    const uint8_t addrs=0xA0;

    //I2C用にピンを設定
    void gpio_conf();

    //I2C3の設定
    void i2c3_conf();
    
    //スタートコンディションとデバイスアドレスの送信
    //direction:書き込みか読み込みかの選択
    void i2c3_start(uint8_t addrs,uint8_t direction);
    
    //8bitのデータを送信
    //data:送信するデータ
    void i2c3_write(uint8_t data);

    //EEPROMからデータを受信しACKを返す
    uint8_t i2c3_read_ack();

    //EEPROMからデータを受信しACKを返さない 
    uint8_t i2c3_read_nack();
  
    //stopコンディションの送信
    void i2c3_stop();
    
    //データの送受信が正常に終了したかチェックする.
    //エラーの場合はcallback関数を呼び出す
    //register_flag:確認するI2C_FLAG
    void i2c_check_bus_statas(uint32_t register_flag);

    //通信エラーのときに呼び出す
    uint8_t timeout_user_callback();
  public:
    EEPROM(){}
    ~EEPROM(){}

    //dataの送信
    //page_addr:eepromのページアドレス(17bit)
    //data:送信するデータ
    //data_size:送信するデータの配列の要素数
    //return:0送信失敗 1:送信成功
    uint8_t write(uint32_t page_addr,uint8_t* data,uint16_t data_size);

    //dataの受信
    //page_addr:eepromのページアドレス
    //data:受信したデータを書き込む配列
    //data_size:受信するデータの要素数
    //return: 0:受信失敗　1:受信成功
    uint8_t read(uint32_t page_addr,uint8_t* data,uint16_t data_size);
};
