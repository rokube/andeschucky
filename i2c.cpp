#include "stm32f4xx.h"
#include "stm32f4xx_conf.h"
#include "usart.h"
#include "i2c.h"

void EEPROM::gpio_conf(){
  GPIO_InitTypeDef gpio_struct;

  //eepromに使うGPIOのクロックを有効
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA,ENABLE);
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC,ENABLE);

  //PA8をSCLに設定
  gpio_struct.GPIO_Pin=GPIO_Pin_8;
  gpio_struct.GPIO_Mode=GPIO_Mode_AF;
  gpio_struct.GPIO_Speed=GPIO_Speed_50MHz;
  gpio_struct.GPIO_OType=GPIO_OType_OD;
  gpio_struct.GPIO_PuPd=GPIO_PuPd_UP;

  GPIO_Init(GPIOA,&gpio_struct);

  //PC9をSDAに設定
  gpio_struct.GPIO_Pin=GPIO_Pin_9;
  GPIO_Init(GPIOC,&gpio_struct);

  //PC8をGPIOに設定
  gpio_struct.GPIO_Pin=GPIO_Pin_8;
  gpio_struct.GPIO_Mode=GPIO_Mode_OUT;
  gpio_struct.GPIO_Speed=GPIO_Speed_50MHz;
  gpio_struct.GPIO_OType=GPIO_OType_PP;
  gpio_struct.GPIO_PuPd=GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOC,&gpio_struct);

  //eepromの書き込み・読み込みを有効
  GPIO_ResetBits(GPIOC,GPIO_Pin_8);

  GPIO_PinAFConfig(GPIOA,GPIO_PinSource8,GPIO_AF_I2C3);
  GPIO_PinAFConfig(GPIOC,GPIO_PinSource9,GPIO_AF_I2C3);
}

void EEPROM::i2c3_conf(){
  I2C_InitTypeDef i2c_struct;

  i2c_struct.I2C_ClockSpeed=100000;
  i2c_struct.I2C_Mode=I2C_DutyCycle_2;
  i2c_struct.I2C_DutyCycle=I2C_DutyCycle_2;
  i2c_struct.I2C_OwnAddress1=0x00;
  i2c_struct.I2C_Ack=I2C_Ack_Disable;
  i2c_struct.I2C_AcknowledgedAddress=I2C_AcknowledgedAddress_7bit;

  I2C_Init(I2C3,&i2c_struct);

  I2C_Cmd(I2C3,ENABLE);
}

void EEPROM::i2c3_start(uint8_t addrs,uint8_t direction){
  while(I2C_GetFlagStatus(I2C3,I2C_FLAG_BUSY));
  I2C_GenerateSTART(I2C3,ENABLE);
  while(!I2C_CheckEvent(I2C3,I2C_EVENT_MASTER_MODE_SELECT));
  I2C_Send7bitAddress(I2C3,addrs,direction);
  if(direction==I2C_Direction_Transmitter){
    while(!I2C_CheckEvent(I2C3,I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED)); 
  }
  else if(direction==I2C_Direction_Receiver){
    while(!I2C_CheckEvent(I2C3,I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED)); 
  }
}

void EEPROM::i2c3_write(uint8_t data){
  I2C_SendData(I2C3,data);
  while(!I2C_CheckEvent(I2C3,I2C_EVENT_MASTER_BYTE_RECEIVED));
}

uint8_t EEPROM::i2c3_read_ack(){
  I2C_AcknowledgeConfig(I2C3,ENABLE);
  while(!I2C_CheckEvent(I2C3,I2C_EVENT_MASTER_BYTE_RECEIVED));
  uint8_t data=I2C_ReceiveData(I2C3);
  return data;
}

uint8_t EEPROM::i2c3_read_nack(){
  I2C_AcknowledgeConfig(I2C3,DISABLE);
  I2C_GenerateSTOP(I2C3,ENABLE);
  while(!I2C_CheckEvent(I2C3,I2C_EVENT_MASTER_BYTE_RECEIVED));
  uint8_t data=I2C_ReceiveData(I2C3);

  return data;
}


void EEPROM::i2c3_stop(){
  I2C_GenerateSTOP(I2C3,ENABLE);
}

uint8_t EEPROM::write(uint32_t page_addr,uint8_t* data,uint16_t data_size){
  //eepromのDevice Addrsの計算 
  uint8_t d_addrs=(uint8_t)(addrs|((page_addr>>15)&0x02));

  i2c3_start(d_addrs,I2C_Direction_Transmitter);

  //送信するページアドレス
  uint8_t f_addrs=(uint8_t)((page_addr&0xFF00)>>8);
  uint8_t s_addrs=(uint8_t)(page_addr&0x00FF);

  i2c3_write(f_addrs);
  i2c3_write(s_addrs);

  //データの送信
  for(uint16_t i=0;i<data_size;i++){
    i2c3_write(*(data+i)); 
  }  

  i2c3_stop();

  return 1;
}

uint8_t EEPROM::read(uint32_t page_addr,uint8_t* data,uint16_t data_size){
  uint8_t d_addrs=(uint8_t)(addrs|((page_addr>>15)&0x02));

  i2c3_start(d_addrs,I2C_Direction_Transmitter);

  uint8_t f_addrs=(uint8_t)((page_addr&0xFF00)>>8);
  uint8_t s_addrs=(uint8_t)(page_addr&0x00FF);

  i2c3_write(f_addrs);
  i2c3_write(s_addrs);

  I2C_GenerateSTART(I2C3,ENABLE);
  while(!I2C_CheckEvent(I2C3,I2C_EVENT_MASTER_MODE_SELECT));
  I2C_Send7bitAddress(I2C3,d_addrs,I2C_Direction_Receiver);
  while(!I2C_CheckEvent(I2C3,I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED));

  for(uint16_t i=0;i<(data_size-1);i++){
    *(data+i)=i2c3_read_ack();
  }
  *(data+data_size-1)=i2c3_read_nack();
  i2c3_stop();

  return 1;

  i2c3_stop();


}

